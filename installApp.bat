@echo off
call git pull origin develop
cd ./server
rmdir dist /s /q
call npm i --legacy-peer-deps
call npm run build
call npm install forever -g
cd ../web-client
rmdir dist /s /q
call npm i --legacy-peer-deps
call npm run build
call npm i -g angular-http-server
