@echo off
start /b mongod --dbpath ../bdd/production/
call forever stopall
call forever start ./server/dist/app.js
start /b call angular-http-server --path ./web-client/dist/app-farmacia -p 8081 -g --cors -i -o
sleep 5
start /b chrome http://localhost:8081
