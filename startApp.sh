#!/bin/bash
nohup mongod --dbpath ../bdd/production/ > /dev/null 2>&1 &
forever stopall
forever start ./server/dist/app.js
nohup angular-http-server --path ./web-client/dist/app-farmacia -p 8081 --cors -i -o > /dev/null 2>&1 &
sleep 5
open -a "Google Chrome" http://localhost:8081
