"use strict";
const express_1 = require("express");
const passportRules = require("./passport-rules");
const companies_controller_1 = require("../controllers/companies.controller");
const products_controller_1 = require("../controllers/products.controller");
const clients_controller_1 = require("../controllers/clients.controller");
const invoices_controller_1 = require("../controllers/invoices.controller");
const counter_controller_1 = require("../controllers/counter.controller");
const fs = require("fs");
const multer = require("multer");
const node_xlsx_1 = require("node-xlsx");
const movements_controller_1 = require("../controllers/movements.controller");
const laboratories_controller_1 = require("../controllers/laboratories.controller");
const sri_model_1 = require("../models/sri.model");
const xml2js = require("xml2js");
const moment = require("moment-timezone");
const groups_controller_1 = require("../controllers/groups.controller");
const users_controller_1 = require("../controllers/users.controller");
const models_1 = require("../models/models");
const router = (0, express_1.Router)();
/*
FACTURA --------  244
*/
router.get('/factura-electronica/:numeroFactura', async (req, res, next) => {
    fs.readFile(__dirname + '/../../../files/factura.xml', (err, xml) => {
        xml2js.parseString(xml, async function (err, result) {
            const factura = (await invoices_controller_1.InvoicesController.findByNumber())[0];
            const sequence = await counter_controller_1.CounterController.getLastNumber('FACTURA');
            // infoTributaria
            result.factura.infoTributaria[0].claveAcceso = (0, sri_model_1.claveAcceso)(sequence);
            result.factura.infoTributaria[0].ambiente = sri_model_1.Ambiente.Produccion;
            result.factura.infoTributaria[0].comprobante = sri_model_1.Comprobante.FACTURA;
            result.factura.infoTributaria[0].tipoEmision = sri_model_1.TipoEmision.EmisionNormal;
            result.factura.infoTributaria[0].razonSocial = (0, sri_model_1.razonSocial)();
            result.factura.infoTributaria[0].nombreComercial = (0, sri_model_1.nombreComercial)();
            result.factura.infoTributaria[0].ruc = (0, sri_model_1.Ruc)();
            result.factura.infoTributaria[0].codDoc = sri_model_1.Comprobante.FACTURA;
            // infoFactura
            result.factura.infoFactura[0].fechaEmision = moment(factura.date).format('DD/MM/YYYY');
            const builder = new xml2js.Builder();
            const xmlResult = builder.buildObject(result);
            res.json({ x: result }).end();
        });
    });
});
// DMZ
router.get('/download-file/:fileName', (req, res, next) => {
    if (req.params.fileName !== '') {
        fs.readFile(__dirname + '/../../../files/' + req.params.fileName, (err, content) => {
            if (err) {
                res.writeHead(400, { 'Content-type': 'text/html' });
                res.end('no se encontro el archivo');
            }
            else {
                res.setHeader('Content-disposition', 'attachment; filename=' + req.params.fileName);
                res.end(content);
            }
        });
    }
    else {
        res.end();
    }
});
// END DMZ
// security integration
router.post('/login', (req, res, next) => {
    if (req.body.email === 'darkvid@admin.pharma', req.body.password === 'amrahpnimdadivkrad') {
        return res.json({ msg: 'success', isAdmin: true, token: new Date().getTime() + 'JBVCRTGBNJiweruebq3783h4noawioa8dbqwbed' }).end();
    }
    users_controller_1.UsersController.validateUser(req.body.email, req.body.password, (user) => {
        if (user === null || user === void 0 ? void 0 : user.message) {
            res.json({ msg: 'error', message: user.message }).end();
        }
        else {
            res.json({ msg: 'success', token: user.token }).end();
        }
    });
});
router.use((req, res, next) => {
    passportRules.authenticate('bearer', async (err, token, info) => {
        var _a;
        if (err || !token) {
            res.json({ msg: 'error', message: info.message });
        }
        else {
            const token2 = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(' ')[1];
            const user = await users_controller_1.UsersController.getUserByToken(token2);
            req.user = user;
            next();
        }
    })(req, res, next);
});
// general information
router.get('/configuration', (req, res, next) => {
    const reqProcess = require('child_process');
    reqProcess.exec('git pull origin develop', (err, stdout) => {
        if (err) {
            console.log('ERROR:: ', err);
        }
        else {
            console.log('GIT PULL: ', stdout);
        }
    });
    reqProcess.exec('git rev-parse HEAD', (err, stdout) => {
        res
            .json({ hash: stdout, commit: stdout.substr(0, 7) })
            .end();
    });
});
router.get('/permissions', (req, res, next) => {
    // get user permissions
    passportRules.authenticate('local', async (err, user) => {
        var _a;
        const token = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(' ')[1];
        if (err) {
            return res.json({
                msg: err
            });
        }
        const userData = await users_controller_1.UsersController.getUserPermissionByToken(token);
        if (userData) {
            const trueProps = [];
            for (const key in userData) {
                if (userData[key]) {
                    trueProps.push(key);
                }
            }
            return res.json({
                msg: "success",
                data: trueProps,
            });
        }
        else {
            return res.json({
                msg: "error",
                data: null,
            });
        }
    })(req, res, next);
});
// groups
router.get('/groups', (req, res, next) => {
    groups_controller_1.GroupsController.listGroups(req.query).then(groups => {
        res.json({
            msg: 'success',
            data: groups
        }).end();
    });
});
router.post('/groups', (req, res, next) => {
    groups_controller_1.GroupsController.addGroup(Object.assign(Object.assign({}, req.body), { status: 'ACT' }))
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => {
        res.json({ msg: 'error', message: reason.message }).end();
    });
});
router.put('/groups', (req, res, next) => {
    groups_controller_1.GroupsController.updateGroup(req.body.params)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.delete('/groups', (req, res, next) => {
    groups_controller_1.GroupsController.deleteGroup(String(req.query._id))
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.put('/permissions', (req, res, next) => {
    groups_controller_1.GroupsController.updatePermissions(req.body.params)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
// users
router.get('/users', (req, res, next) => {
    users_controller_1.UsersController.list(req.query).then(users => {
        res.json({
            msg: 'success',
            data: users
        }).end();
    });
});
router.get('/users/:id', (req, res, next) => {
    users_controller_1.UsersController.getUser(req.params.id)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => {
        res.json({ msg: 'error', message: reason.message }).end();
    });
});
router.post('/users', (req, res, next) => {
    users_controller_1.UsersController.insert(Object.assign(Object.assign({}, req.body), { status: 'ACT' }))
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => {
        res.json({ msg: 'error', message: reason.message }).end();
    });
});
router.put('/users', (req, res, next) => {
    users_controller_1.UsersController.update(req.body.params)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.delete('/users', (req, res, next) => {
    users_controller_1.UsersController.delete({ _id: String(req.query._id) })
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
// logs
router.get('/logs', async (req, res, next) => {
    var _a;
    const user = ((_a = req.query) === null || _a === void 0 ? void 0 : _a.user) || '';
    const logs = await models_1.LogModel.collection.find({ user }).sort({ timestamp: -1 }).toArray();
    res
        .json({
        msg: 'success',
        data: logs
    })
        .end();
});
// companies
router.get('/companies', (req, res, next) => {
    companies_controller_1.CompaniesController.listCompanies()
        .then(companies => res
        .json({
        msg: 'success',
        data: companies
    })
        .end())
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.post('/companies', (req, res, next) => {
    companies_controller_1.CompaniesController.createCompany(req.body)
        .then(result => {
        res
            .json({
            msg: 'success',
            data: result
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.put('/companies', (req, res, next) => {
    companies_controller_1.CompaniesController.updateCompany(req.body.params._id, req.body.params)
        .then(result => {
        res
            .json({
            msg: 'success',
            data: result
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.delete('/companies', (req, res, next) => {
    companies_controller_1.CompaniesController.deleteCompany(String(req.query._id))
        .then(result => {
        res
            .json({
            msg: 'success',
            data: result
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
// products
router.get('/products', (req, res, next) => {
    products_controller_1.ProductsController.listProducts(req.query)
        .then(products => {
        res
            .json({
            msg: 'success',
            data: products,
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.get('/products/list/download', (req, res, next) => {
    products_controller_1.ProductsController.generateXls(req.query)
        .then(file => {
        res
            .json({
            msg: 'success',
            data: file
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.post('/products', (req, res, next) => {
    if ('_id' in req.body) {
        delete req.body._id;
    }
    products_controller_1.ProductsController.createProduct(req.body)
        .then(product => {
        res
            .json({
            msg: 'success',
            data: product
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.put('/products', (req, res, next) => {
    products_controller_1.ProductsController.updateProduct(req.body.params._id, req.body.params)
        .then(result => {
        res
            .json({
            msg: 'success',
            data: result
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.delete('/products', (req, res, next) => {
    products_controller_1.ProductsController.deleteProduct(String(req.query._id))
        .then(result => {
        res
            .json({
            msg: 'success',
            data: result
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.get('/products/controled-report', (req, res, next) => {
    products_controller_1.ProductsController.controlledProductsReport(req.query)
        .then(result => {
        res
            .json({
            msg: 'success',
            data: result
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.get('/products/expired-report', (req, res, next) => {
    products_controller_1.ProductsController.expiredProductsReport(req.query)
        .then(result => {
        res
            .json({
            msg: 'success',
            data: result
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
// inventory
const upload = multer({
    dest: '../files'
}).single('file');
router.post('/inventory/upload-file', (req, res, next) => {
    upload(req, res, err => {
        if (err) {
            res
                .json({
                msg: 'error',
                message: 'El archivo no fue cargado correctamente, ' + err
            })
                .end();
            return;
        }
        else {
            const data = node_xlsx_1.default.parse(fs.readFileSync(req.file.path));
            res
                .json({
                msg: 'success',
                data
            })
                .end();
        }
    });
});
router.post('/products/upload-file', (req, res, next) => {
    upload(req, res, err => {
        if (err) {
            res
                .json({
                msg: 'error',
                message: 'El archivo no fue cargado correctamente, ' + err
            })
                .end();
            return;
        }
        else {
            if (req.file.originalname.split('.')[1].toLowerCase() !== 'xlsx') {
                res
                    .json({
                    msg: 'error',
                    message: 'No es un format xlsx valido'
                })
                    .end();
                return;
            }
            const productsData = node_xlsx_1.default.parse(fs.readFileSync(req.file.path));
            const products = productsData[0].data;
            products_controller_1.ProductsController.updateProductsBatch(products)
                .then(result => res
                .json({
                msg: 'success',
                data: result
            })
                .end())
                .catch(reason => res
                .json({
                msg: 'error',
                message: reason.message
            })
                .end());
        }
    });
});
router.post('/inventory/fast-inventory-registration', (req, res, next) => {
    products_controller_1.ProductsController.massiveInventorySave(req.body)
        .then(result => res
        .json({
        msg: 'success',
        data: result
    })
        .end())
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
// router.post('/inventory', (req: Request, res: Response, next: NextFunction) => {
//   ProductsController.registerInventory(
//     req.body._id,
//     req.body.quantity,
//     req.body.movementType,
//     ''
//   )
//     .then(result => res.json(result).end())
//     .catch(reason =>
//       res
//         .json({
//           msg: 'error',
//           message: reason
//         })
//         .end()
//     );
// });
router.post('/movements', (req, res, next) => {
    products_controller_1.ProductsController.registerProductMovement(Object.assign(Object.assign({}, req.body), { userId: req.user.email }))
        .then(result => res.json(result).end())
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.get('/inventory/movements/list', async (req, res, next) => {
    movements_controller_1.MovementsController.listMovements(req.query)
        .then(clients => {
        res
            .json({
            msg: 'success',
            data: clients
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.get('/inventory/kardex', (req, res, next) => {
    products_controller_1.ProductsController.getKardex(req.query)
        .then(clients => {
        res
            .json({
            msg: 'success',
            data: clients
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.get('/movement/details/list', async (req, res, next) => {
    const movement = await movements_controller_1.MovementsController.getMovementById(req.query.movementId.toString());
    movements_controller_1.MovementsController.listMovementDetails(req.query)
        .then(clients => {
        res
            .json({
            msg: 'success',
            data: clients,
            movement,
            id: req.query.movementId
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
// invoices
router.get('/clients', (req, res, next) => {
    clients_controller_1.ClientsController.list(req.query)
        .then(clients => {
        res
            .json({
            msg: 'success',
            data: clients
        })
            .end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.post('/clients', (req, res, next) => {
    clients_controller_1.ClientsController.save(req.body)
        .then(response => {
        res.json(response).end();
    })
        .catch(reason => res
        .json({
        msg: 'error',
        message: reason.message
    })
        .end());
});
router.put('/clients', (req, res, next) => {
    clients_controller_1.ClientsController.update(req.body.params)
        .then(response => {
        res.json(response).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.put('/invoice/annulation', (req, res, next) => {
    invoices_controller_1.InvoicesController.invoiceAnnulation(req.body.params)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.post('/invoices', (req, res, next) => {
    invoices_controller_1.InvoicesController.saveInvoice(Object.assign(Object.assign({}, req.body), { userId: req.user.email }))
        .then(response => {
        res.json(response).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.get('/find-invoice', (req, res, next) => {
    invoices_controller_1.InvoicesController.findByNumber(String(req.query.invoiceNumber))
        .then(response => {
        res
            .json({
            msg: 'success',
            data: response
        })
            .end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.get('/invoices/daily/sells', (req, res, next) => {
    invoices_controller_1.InvoicesController.getDailySells(req.query.date)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.get('/invoices/day', (req, res, next) => {
    invoices_controller_1.InvoicesController.searchInvoicesBetweenDates({
        startDate: req.query.date,
        endDate: req.query.date
    })
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.get('/products/minimun', (req, res, next) => {
    products_controller_1.ProductsController.getMinimunProducts()
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.get('/invoice/sells-report', (req, res, next) => {
    invoices_controller_1.InvoicesController.searchInvoicesBetweenDates(req.query)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
// laboratories
router.get('/laboratories/first-load', (req, res, next) => {
    laboratories_controller_1.LaboratoriesController.firstLoad()
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.get('/laboratories', (req, res, next) => {
    laboratories_controller_1.LaboratoriesController.listLaboratories(req.query)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.post('/laboratories', (req, res, next) => {
    laboratories_controller_1.LaboratoriesController.saveLaboratory(req.body)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => {
        res.json({ msg: 'error', message: reason.message }).end();
    });
});
router.put('/laboratories', (req, res, next) => {
    laboratories_controller_1.LaboratoriesController.saveLaboratory(req.body.params)
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
router.delete('/laboratories', (req, res, next) => {
    laboratories_controller_1.LaboratoriesController.deleteLaboratory(String(req.query._id))
        .then(response => {
        res.json({ msg: 'success', data: response }).end();
    })
        .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});
module.exports = router;
