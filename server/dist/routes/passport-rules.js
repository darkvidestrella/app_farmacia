"use strict";
const passport = require("passport");
const passportLocal = require("passport-local");
const passportLocalToken = require("passport-bearer-strategy");
const users_controller_1 = require("../controllers/users.controller");
const LocalStrategy = passportLocal.Strategy;
const TokenStrategy = passportLocalToken.Strategy;
passport.serializeUser((user, cb) => {
    cb(undefined, user._id);
});
// passport.deserializeUser(function (id, cb) {
//   db.users.findById(id, function (err, user) {
//     if (err) { return cb(err); }
//     cb(null, user);
//   });
// });
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false,
    passReqToCallback: false
}, (email, password, done) => {
    users_controller_1.UsersController.validateUser(email, password, user => {
        if ('_id' in user) {
            return done(undefined, user);
        }
        else {
            return done(user.message);
        }
    });
}));
passport.use('bearer', new TokenStrategy({
    passReqToCallback: true,
    missingTokenMessage: 'No authorization header'
}, (req, token, done) => {
    return done(undefined, {
        token
    });
}));
module.exports = passport;
