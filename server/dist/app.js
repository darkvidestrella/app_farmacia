"use strict";
const express = require("express");
const expressSession = require("express-session");
const mongoose = require("mongoose");
const cors = require("cors");
const passport = require("passport");
const routes = require("./routes/router");
const apiRoutes = require("./routes/api-router");
const configuration_1 = require("./configuration");
const log_middleware_1 = require("./middleware/log.middleware");
class Server {
    static bootstrap() {
        return new Server();
    }
    constructor() {
        this.app = express();
        this.app.set('port', 3001);
        this.app.use(express.json({ limit: '100mb' }));
        this.app.use(log_middleware_1.default);
        this.app.use(express.urlencoded({ limit: '100mb' }));
        this.app.use(cors());
        this.app.use(expressSession({
            secret: 'pharmafarmasec',
            cookie: {}
        }));
        this.app.use(passport.initialize());
        this.app.use(passport.session());
        this.app.use('/auth', routes);
        // secure path
        this.app.use('/api', apiRoutes);
        this.app.listen(this.app.get('port'), () => {
            this.startMongoConnection();
            console.log('App is running at http://localhost:%d in %s mode bdd: ' + configuration_1.CONFIG.bdd, this.app.get('port'), this.app.get('env'));
            console.log('Press CTRL-C to stop\n');
        });
    }
    startMongoConnection() {
        console.log('Try connection to: ', `mongodb://${configuration_1.CONFIG.bdd}/farmacia_bdd`);
        mongoose.connect(`mongodb://${configuration_1.CONFIG.bdd}/farmacia_bdd`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        }).then(() => {
            console.log('Mongo db connected');
        }).catch(() => {
            setTimeout(() => {
                this.startMongoConnection();
            }, 5000);
        });
    }
    lockApplication(req, res, next) {
        if (true) {
            next();
        }
        else {
            res.json({
                access: 'Unauthorized'
            }).end();
        }
    }
}
module.exports = Server.bootstrap().app;
