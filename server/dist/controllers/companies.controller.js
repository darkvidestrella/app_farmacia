"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompaniesController = void 0;
const models_1 = require("../models/models");
class CompaniesController {
    async listCompanies() {
        return await models_1.CompaniesModel.collection.find().toArray();
    }
    async createCompany(company) {
        const newCompany = await models_1.CompaniesModel.create(company);
        return newCompany;
    }
    async updateCompany(_id, company) {
        return models_1.CompaniesModel.findByIdAndUpdate({ _id }, company);
    }
    deleteCompany(_id) {
        return models_1.CompaniesModel.deleteOne({
            _id
        });
    }
}
const companiesController = new CompaniesController();
exports.CompaniesController = companiesController;
