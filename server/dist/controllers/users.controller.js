"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
const models_1 = require("../models/models");
const mongoose = require("mongoose");
class UsersController {
    async getUser(_id) {
        const user = await models_1.UserModel.collection.findOne({
            _id: mongoose.Types.ObjectId(_id)
        });
        delete user.password;
        return user;
    }
    async getUserByToken(token) {
        const user = await models_1.UserModel.collection.findOne({
            user: {
                token,
            }
        });
        if (user) {
            delete user.password;
            return user;
        }
        else {
            return null;
        }
    }
    async getUserPermissionByToken(token) {
        const user = await this.getUserByToken(token);
        if (user) {
            delete user.password;
            const groupData = await models_1.GroupModel.collection.findOne({
                _id: mongoose.Types.ObjectId(user.group),
            });
            return JSON.parse(groupData.menu);
        }
        else {
            return [];
        }
    }
    async list(data) {
        return await models_1.UserModel.collection.find({}, { projection: { password: 0 } }).toArray();
    }
    async insert(data) {
        const newUser = await models_1.UserModel.collection.insertOne(data);
        if (newUser.result.ok) {
            return newUser.ops[0];
        }
        return undefined;
    }
    async update(data) {
        const userData = await this.getUser(data._id);
        const newUserData = Object.assign(Object.assign({}, userData), data);
        delete newUserData._id;
        const updatedUser = await models_1.UserModel.collection
            .findOneAndUpdate({
            _id: mongoose.Types.ObjectId(data._id)
        }, {
            $set: Object.assign({}, newUserData)
        });
        if (updatedUser.ok) {
            return this.getUser(data._id);
        }
        return undefined;
    }
    async delete(data) {
        const lastRecord = await this.getUser(data._id);
        const deleted = await models_1.UserModel.collection.findOneAndDelete({
            _id: mongoose.Types.ObjectId(data._id)
        });
        if (deleted.ok) {
            return lastRecord;
        }
        return undefined;
    }
    validateUser(email, password, cb) {
        models_1.UserModel.collection.findOne({
            email,
            password,
            status: { $ne: 'ELI' }
        }).then(user => {
            if (user) {
                const newToken = `newsecuritytoken-${user._id}`;
                this.update({
                    _id: user._id,
                    user: {
                        token: newToken
                    }
                });
                user.token = newToken;
                cb(user);
            }
            else {
                cb({
                    message: `No se encontro al usuario: ${email}`
                });
            }
        }).catch(reason => {
            cb({ message: reason.message });
        });
    }
}
const users = new UsersController();
exports.UsersController = users;
