"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupsController = void 0;
const models_1 = require("../models/models");
const mongoose = require("mongoose");
class GroupsController {
    async listGroups(params) {
        return models_1.GroupModel.collection
            .find({
            $or: [
                { name: new RegExp(params.filter, 'i') },
            ],
            $and: [{
                    status: 'ACT'
                }]
        })
            .sort({
            name: 1
        })
            .toArray();
    }
    async addGroup(data) {
        const newGroup = await models_1.GroupModel.collection
            .insert(data);
        if (newGroup.result.ok) {
            return newGroup.ops[0];
        }
        else {
            return undefined;
        }
    }
    async updateGroup(data) {
        const updated = await models_1.GroupModel.collection.findOneAndUpdate({
            _id: mongoose.Types.ObjectId(data._id)
        }, {
            $set: {
                name: data.name
            }
        });
        if (updated.ok) {
            return await models_1.GroupModel.collection.findOne({
                _id: mongoose.Types.ObjectId(data._id)
            });
        }
        else {
            return undefined;
        }
    }
    async updatePermissions(data) {
        const updated = await models_1.GroupModel.collection.findOneAndUpdate({
            _id: mongoose.Types.ObjectId(data._id)
        }, {
            $set: {
                menu: data.menu
            }
        });
        if (updated.ok) {
            return await models_1.GroupModel.collection.findOne({
                _id: mongoose.Types.ObjectId(data._id)
            });
        }
        else {
            return undefined;
        }
    }
    async deleteGroup(id) {
        const updated = await models_1.GroupModel.collection.findOneAndUpdate({
            _id: mongoose.Types.ObjectId(id)
        }, {
            $set: { status: "ELI" }
        });
        if (updated.ok) {
            return await models_1.GroupModel.collection.findOne({
                _id: mongoose.Types.ObjectId(id)
            });
        }
        else {
            return undefined;
        }
    }
}
const groupsController = new GroupsController();
exports.GroupsController = groupsController;
