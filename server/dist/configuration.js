"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CONFIG = void 0;
exports.CONFIG = {
    timezone: 'America/Guayaquil',
    bdd: process.env.BDD || '127.0.0.1:27017'
};
