"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateTime = void 0;
const moment = require("moment-timezone");
const configuration_1 = require("../configuration");
class DateTime {
    constructor(date) {
        this.date = moment(date
            ? date
            : new Date().toISOString()).tz(configuration_1.CONFIG.timezone);
    }
    format(dateFormat) {
        return this.date.format(dateFormat);
    }
}
exports.DateTime = DateTime;
