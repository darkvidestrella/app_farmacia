import * as moment from 'moment-timezone';
import { CONFIG } from '../configuration';

export class DateTime {
  date: moment.Moment;

  constructor(date?: string) {
    this.date = moment(
      date
      ? date
      : new Date().toISOString()
    ).tz(CONFIG.timezone);
  }

  format(dateFormat?: string) {
    return this.date.format(dateFormat);
  }
}
