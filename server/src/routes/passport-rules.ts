import * as passport from 'passport';
import * as passportLocal from 'passport-local';
import * as passportLocalToken from 'passport-bearer-strategy';

import { UsersController } from '../controllers/users.controller';

const LocalStrategy = passportLocal.Strategy;
const TokenStrategy = passportLocalToken.Strategy;

passport.serializeUser((user: any, cb) => {
  cb(undefined, user._id);
});

// passport.deserializeUser(function (id, cb) {
//   db.users.findById(id, function (err, user) {
//     if (err) { return cb(err); }
//     cb(null, user);
//   });
// });

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      session: false,
      passReqToCallback: false
    },
    (email, password, done) => {
      UsersController.validateUser(email, password, user => {
        if ('_id' in user) {
          return done(undefined, user);
        } else {
          return done(user.message);
        }
      });
    }
  )
);

passport.use(
  'bearer',
  new TokenStrategy({
    passReqToCallback: true,
    missingTokenMessage: 'No authorization header'
  }, (req, token, done) => {
    return done(undefined, {
      token
    });
  })
);

export = passport;
