import { Router } from 'express';

import * as passportRules from './passport-rules';

import * as Users from '../app/users';

const router: Router = Router();

router.post(
  '/login',
  (req, res, next) => {
    passportRules.authenticate('local', (err, user) => {
      if (err) {
        return res.json({
          msg: err
        });
      }
      Users.processLogin(req, res, next);
    })(req, res, next);
  }
);

router.get(
  '/restricted',
  (req, res, next) => {
    passportRules.authenticate('bearer', (err, token, info) => {
      if (!token) {
        return res.json({
          error: info.message
        }).end();
      }
      res.json({
        access: 'token ok'
      }).end();
    })(req, res, next);
  }
);

export = router;
