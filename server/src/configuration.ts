export const CONFIG = {
  timezone: 'America/Guayaquil',
  bdd: process.env.BDD || '127.0.0.1:27017'
};
