import { Request, Response, NextFunction } from 'express';
import { LogModel } from '../models/models';

const logMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  res.on('finish', async () => {
    if (req.originalUrl === '/api/permissions' && !req.user) {
      console.error('No se encontro al usuario');
    } else if (req.method.toLowerCase() !== 'options') {
      try {
        const logData = {
          user: req.user ? req.user._id : 'anonimo',
          endpoint: req.originalUrl,
          method: req.method,
          statusCode: res.statusCode,
          requestData: {body: req.body, params: req.params, query: req.query, user: req.user},
          timestamp: new Date().toISOString(),
        };

        await LogModel.create(logData);
      } catch (error) {
        console.error('Error al registrar en la bitácora:', error);
      }
    }
  });

  next();
};

export default logMiddleware;
