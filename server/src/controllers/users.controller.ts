import { GroupModel, UserModel } from '../models/models';
import * as mongoose from 'mongoose';

class UsersController {

  async getUser(_id: string) {
    const user = await UserModel.collection.findOne({
      _id: mongoose.Types.ObjectId(_id)
    });
    delete user.password;
    return user;
  }

  async getUserByToken(token: string) {
    const user = await UserModel.collection.findOne({
      user: {
        token,
      }
    });
    if (user) {
      delete user.password;
      return user;
    } else {
      return null;
    }
  }

  async getUserPermissionByToken(token: string) {
    const user = await this.getUserByToken(token);
    if (user) {
      delete user.password;
      const groupData = await GroupModel.collection.findOne({
        _id: mongoose.Types.ObjectId(user.group),
      })
      return JSON.parse(groupData.menu);
    } else {
      return [];
    }
  }

  async list(data) {
    return await UserModel.collection.find({}, { projection: { password: 0 } }).toArray();
  }

  async insert(data) {
    const newUser = await UserModel.collection.insertOne(data);
    if (newUser.result.ok) {
      return newUser.ops[0];
    }
    return undefined;
  }

  async update(data) {
    const userData = await this.getUser(data._id);
    const newUserData = {
      ...userData,
      ...data,
    };

    delete newUserData._id;
    const updatedUser = await UserModel.collection
      .findOneAndUpdate({
        _id: mongoose.Types.ObjectId(data._id)
      }, {
        $set: { ...newUserData }
      }
    );
    if (updatedUser.ok) {
      return this.getUser(data._id);
    }
    return undefined;
  }

  async delete(data) {
    const lastRecord = await this.getUser(data._id);
    const deleted = await UserModel.collection.findOneAndDelete({
      _id: mongoose.Types.ObjectId(data._id)
    });
    if (deleted.ok) {
      return lastRecord;
    }
    return undefined;
  }

  validateUser(email: string, password: string, cb: any) {
    UserModel.collection.findOne({
      email,
      password,
      status: { $ne: 'ELI' }
    }).then(user => {
      if (user) {
        const newToken = `newsecuritytoken-${user._id}`;
        this.update({
          _id: user._id,
          user: {
            token: newToken
          }
        });
        user.token = newToken;
        cb(user);
      } else {
        cb({
          message: `No se encontro al usuario: ${email}`
        });
      }
    }).catch(reason => {
      cb({message: reason.message});
    });
  }
}

const users = new UsersController();
export { users as UsersController };
