import { MovementModel, MovementDetailModel, ProductModel } from '../models/models';
import { DateTime } from '../libraries/datetime';
import * as mongoose from 'mongoose';

class MovementsController {

  async listMovements(params) {
    const startDate = new DateTime(new Date(params.startDate).toISOString()).date.startOf('day');
    const endDate = new DateTime(new Date(params.endDate).toISOString()).date.endOf('day');
    const movementTypes = (params.movementType || '').split(",");
    return MovementModel.collection.find({
      $and: [
        {
          creationDate: {
            $gte: startDate.format(),
            $lte: endDate.format()
          }
        },
        {
          movementType: {$in: movementTypes}
        }
      ]
    }).sort({
      creatonDate: -1,
      movementCode: -1
    }).toArray();
  }

  async listMovementDetails(params) {
    const movements = await MovementDetailModel.collection.find({
      movementId: params.movementId
    }).toArray();
    const details = movements.map(async (detail) => {
      const product = await ProductModel.collection.findOne({
        _id: mongoose.Types.ObjectId(detail.productId)
      });
      return await {
        ...detail,
        product
      };
    });
    return Promise.all(details);
  }

  async getMovementById(movementId: string) {
    return await MovementModel.collection.findOne({ _id: mongoose.Types.ObjectId(movementId) });
  }

}

const movementsController = new MovementsController();
export { movementsController as MovementsController };
