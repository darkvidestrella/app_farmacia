import {
  CounterModel,
} from '../models/models';

class CounterController {

  async getLastNumber(name: string) {
    let sequenceNumber = 0;
    const counterInfo = await CounterModel.collection.find({name}).limit(1).toArray();
    if (counterInfo && counterInfo.length > 0) {
      sequenceNumber = counterInfo[0].sequence + 1;
    } else {
      sequenceNumber = 1;
    }
    return sequenceNumber.toString().padStart(9, '0');
  }

  async updateCounter(type: string, sequence: number) {
    return;
  }
}

const counterController = new CounterController();
export { counterController as CounterController };
