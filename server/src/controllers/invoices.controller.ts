import {
  InvoiceModel,
  ProductModel,
  InvoiceDetailsModel
} from '../models/models';
import { ProductsController } from './products.controller';
import * as mongoose from 'mongoose';
import { DateTime } from '../libraries/datetime';

class InvoicesController {
  async createInvoiceSecuence() {
    const date = new DateTime();
    let secuence = '';
    let quantity = await InvoiceModel.countDocuments({
      date: {
        $gte: new DateTime(
          date.format('YYYY') + `-01-01T00:00:00.000${date.format('Z')}`
        ).format(),
        $lte: new DateTime(
          date.format('YYYY') + `-12-31T23:59:59.999${date.format('Z')}`
        ).format()
      }
    });
    quantity = quantity + 1;
    if (quantity < 10) {
      secuence = `00${quantity}`;
    } else if (quantity >= 10 && quantity < 100) {
      secuence = `0${quantity}`;
    } else {
      secuence = `${quantity}`;
    }
    return `FACT-${new DateTime().format('YYYY')}-${secuence}`;
  }

  async saveInvoice(invoice) {
    invoice.date = new DateTime().format();
    const invoiceData = {
      ...invoice,
      date: new DateTime().format(),
      status: 'emited',
      invoiceNumber: await this.createInvoiceSecuence()
    };
    const newInvoice = await InvoiceModel.create(invoiceData);
    if (newInvoice._id) {
      // update inventory
      for (const product of invoice.products) {
        // get product information for a proper transformation of units
        const currentProduct = await ProductModel.collection.findOne({
          _id: mongoose.Types.ObjectId(product._id)
        });
        if (!('quantityPerBox' in currentProduct)) {
          currentProduct.quantityPerBox = 1;
        }

        const totalProductUnits =
          product.boxes * currentProduct.quantityPerBox + product.units;

        ProductsController.registerInventory(
          product._id,
          totalProductUnits,
          'factura',
          ` Factura ID:(${invoiceData.invoiceNumber})`,
          JSON.stringify(newInvoice)
        ).then(() => {
          const productItem = {
            ...product,
            productId: product._id
          };
          delete productItem._id;
          InvoiceDetailsModel.create(productItem);
        });
      }
      return {
        msg: 'success',
        data: newInvoice
      };
    } else {
      return {
        msg: 'error',
        menssage: 'La factura no pudo ser guardada'
      };
    }
  }

  async findByNumber(invoiceNumber?: string) {
    return await InvoiceModel.collection
      .find({
        $or: [
          { invoiceNumber: new RegExp(invoiceNumber, 'i') },
          { 'client.dni': new RegExp(invoiceNumber, 'i') },
          { 'client.firstName': new RegExp(invoiceNumber, 'i') },
          { 'client.lastName': new RegExp(invoiceNumber, 'i') }
        ]
      })
      .sort({ date: -1 })
      .toArray();
  }

  async getDailySells(date) {
    const dayStart = new DateTime();
    const dayEnd = new DateTime();
    const start = dayStart.date.startOf('day');
    const end = dayEnd.date.endOf('day');
    const invoices = await InvoiceModel.collection.find({
      date: {$gte: start.format(), $lte: end.format()},
      status: 'emited'
    }).sort({
      date: 1
    }).toArray();
    return invoices.map(inv => {
      return {
        date: inv.date,
        value: inv.total
      };
    });
  }

  searchInvoicesBetweenDates(params) {
    const startDate = new DateTime(params.startDate).date.startOf('day');
    const endDate = new DateTime(params.endDate).date.endOf('day');
    return InvoiceModel.collection.find({
      date: {
        $gte: startDate.format(),
        $lte: endDate.format()
      }
    }).sort({
      date: -1
    }).toArray();
  }

  async invoiceAnnulation(params) {
    const invoice = await InvoiceModel.collection.findOne({
      _id: mongoose.Types.ObjectId(params._id)
    });
    // restore products inventory
    invoice.products.map(product => {
      const totalProductUnits = (product.boxes * product.quantityPerBox) + product.units;
      ProductsController.registerInventory(
        product._id,
        totalProductUnits,
        'ingreso',
        `Anulacion de factura ID:(${invoice.invoiceNumber})`,
        JSON.stringify(invoice)
      );
    });
    return InvoiceModel.findByIdAndUpdate({
      _id: params._id
    }, {
      status: 'annulated'
    });
  }

}

const invoicesController = new InvoicesController();
export { invoicesController as InvoicesController };
