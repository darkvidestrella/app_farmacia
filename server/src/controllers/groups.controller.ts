import { GroupModel } from '../models/models';
import * as mongoose from 'mongoose';

class GroupsController {
  async listGroups(params) {
    return GroupModel.collection
      .find({
        $or: [
          { name: new RegExp(params.filter, 'i') },
        ],
        $and: [{
          status: 'ACT'
        }]
      })
      .sort({
        name: 1
      })
      .toArray();
  }

  async addGroup(data) {
    const newGroup = await GroupModel.collection
      .insert(data);
    if (newGroup.result.ok) {
      return newGroup.ops[0];
    } else {
      return undefined;
    }
  }

  async updateGroup(data) {
    const updated = await GroupModel.collection.findOneAndUpdate({
      _id: mongoose.Types.ObjectId(data._id)
    }, {
      $set: {
        name: data.name
      }
    });
    if (updated.ok) {
      return await GroupModel.collection.findOne({
        _id: mongoose.Types.ObjectId(data._id)
      });
    } else {
      return undefined;
    }
  }

  async updatePermissions(data) {
    const updated = await GroupModel.collection.findOneAndUpdate({
      _id: mongoose.Types.ObjectId(data._id)
    }, {
      $set: {
        menu: data.menu
      }
    });
    if (updated.ok) {
      return await GroupModel.collection.findOne({
        _id: mongoose.Types.ObjectId(data._id)
      });
    } else {
      return undefined;
    }
  }

  async deleteGroup(id) {
    const updated = await GroupModel.collection.findOneAndUpdate({
      _id: mongoose.Types.ObjectId(id)
    }, {
      $set: {status: "ELI"}
    });
    if (updated.ok) {
      return await GroupModel.collection.findOne({
        _id: mongoose.Types.ObjectId(id)
      });
    } else {
      return undefined;
    }
  }
}

const groupsController = new GroupsController();
export { groupsController as GroupsController };
