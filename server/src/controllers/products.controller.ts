import {
  ProductModel,
  InventoryModel,
  MovementModel,
  MovementDetailModel,
} from "../models/models";
import { DateTime } from "../libraries/datetime";
import * as mongoose from "mongoose";
import * as excel4node from "excel4node";
import { resolve } from "path";

const productColumns = {
  _id: "_id",
  barcode: "Codigo de barras",
  name: "Producto",
  price: "PVP",
  priceDisscount: "Precio con descuento",
  payIva: "Paga Iva (SI/NO)",
  description: "Principio activo",
  prescription: "Prescripcion",
  dosis: "Dosis",
  quantityPerBox: "Cantidad de unidades por caja",
  laboratory: "Laboratorio",
  unitMinimumAlert: "Minimo para alertar",
  restricted: "Necesita receta medica (SI/NO)",
  restrictedType:
    "Tipo / Controlado (precursores, psicotropicos, estupefacientes)",
};

class ProductsController {
  async listProducts(data?: any) {
    let limit = 100;
    let products = [];
    if ("limit" in data) {
      const theLimit = Number(data.limit);
      if (theLimit < 0) {
        limit = 99999999;
      } else {
        limit = theLimit;
      }
    }
    if ("filter" in data) {
      products = await ProductModel.find({
        $and: [
          { status: { $ne: 'ELI' } },
          {
            $or: [
              { barcode: data.filter },
              { name: new RegExp(data.filter, "i") },
              { description: new RegExp(data.filter, "i") },
            ]
          }
        ]
      })
        .sort({
          name: 1,
        })
        .limit(limit);
    } else if ("multiple" in data) {
      products = await ProductModel.collection
        .find({
          $and: [
            {_id: { $in: data.multiple }},
            { status: { $ne: 'ELI' } }
          ]
        })
        .sort({
          name: 1,
        })
        .toArray();
    } else {
      products = await ProductModel.collection.find({$and: [{ status: { $ne: 'ELI' } }]}).limit(limit).toArray();
    }
    // get full inventory product list
    const allInventoryInfo = await this.getAllInventoryInfo(products.map(p => p._id.toString()))

    const inventoriedProducts = products.map(async (product) => {
      // calculate actual inventory
      const inventory = allInventoryInfo?.[product._id.toString()] ? [allInventoryInfo[product._id.toString()]] : [];
      // const inventory = [];
      let actualQty = 0;
      if (inventory.length > 0) {
        actualQty = inventory[0].totalQty;
      }
      let boxes = 0;
      let unit_price = 0;
      let unit_price_disscount = 0;
      if (!product.quantityPerBox) {
        product.quantityPerBox = 1;
      }
      if (product.quantityPerBox > 0) {
        boxes = Math.floor(actualQty / product.quantityPerBox);
        unit_price = product.price / product.quantityPerBox;
        unit_price_disscount =
          (product.priceDisscount || product.price) / product.quantityPerBox;
      }
      const units = actualQty - boxes * product.quantityPerBox;
      const productData = "filter" in data ? product._doc : product;
      // expiration dates
      const movementsDetails = await MovementDetailModel.collection
        .find({
          $and: [
            {productId: product._id.toString()},
            {movementType: 'ingreso'},
            { expirationDate: { $exists: true, $ne: '' } }
          ],
        }).sort({ expirationDate: -1 }).toArray();
      // expiration dates
      return await {
        ...productData,
        boxes,
        units,
        totalUnits: actualQty,
        unit_price: Number(unit_price),
        unit_price_disscount: Number(unit_price_disscount),
        detailedMovements: movementsDetails,
      };
    });
    return Promise.all(inventoriedProducts);
  }

  async createProduct(product) {
    if (!("payIva" in product)) {
      product = {
        ...product,
        payIva: false,
      };
    }
    const newProduct = await ProductModel.create(product);
    const currentDate = new DateTime();
    InventoryModel.create({
      productId: newProduct._id.toString(),
      description: "Creacion de inventario inicial del producto",
      actualQty: 0,
      movedQty: 0,
      totalQty: 0,
      registerDate: currentDate.format(),
      movementType: "Ingreso",
      userId: "yo",
    });
    return newProduct;
  }

  async updateProduct(_id, product) {
    return ProductModel.findByIdAndUpdate({ _id }, product);
  }

  deleteProduct(_id: string) {
    // return ProductModel.deleteOne({
    //   _id,
    // });
    return ProductModel.findByIdAndUpdate({ _id }, { status: 'ELI'});
  }

  async getAllInventoryInfo(productIds: string[]) {
    const latestInventories = await InventoryModel.aggregate([
      {
        $match: {
          productId: { $in: productIds }
        }
      },
      {
        $sort: { registerDate: -1 }
      },
      {
        $group: {
          _id: '$productId',
          latest: { $first: '$$ROOT' }
        }
      },
      {
        $replaceRoot: { newRoot: '$latest' }
      }
    ]).allowDiskUse(true);
    if (latestInventories.length > 0) {
      const inventoryByProductId = latestInventories.reduce((acc, item) => {
        acc[item.productId] = item;
        return acc;
      }, {});

      return inventoryByProductId;
    }
    return [];
  }

  async getInventoryInfo(_id: string): Promise<any> {
    const latestInventory = await InventoryModel.findOne({ productId: _id }).sort({ registerDate: -1 }).lean();
    return [latestInventory];
  }

  async registerInventory(
    _id: string,
    quantity: number,
    movementType: string,
    description?: string,
    additionalInformation?: any
  ) {
    const currentDate = new DateTime();
    // get product information
    const product = await ProductModel.findById(_id).exec();

    // get the last item of the inventory

    let lastRecord = await this.getInventoryInfo(product._id.toString());
    let lastItem = undefined;
    if (lastRecord.length > 0) {
      lastItem = lastRecord[0];
    }
    if (!lastItem) {
      lastItem = await InventoryModel.create({
        productId: product._id,
        description: product["name"],
        actualQty: 0,
        movedQty: 0,
        totalQty: 0,
        registerDate: currentDate.format(),
        movementType: "Ingreso",
        userId: "yo",
      });
    }
    lastRecord = await this.getInventoryInfo(product._id.toString());
    if (lastRecord.length > 0) {
      lastItem = lastRecord[0];
    }
    // capture the current qty
    let currentQty = lastItem.totalQty;
    if (movementType.toLowerCase() === "egreso") {
      if (quantity > currentQty) {
        return {
          msg: "error",
          message: `No hay suficiente stock (Total: ${currentQty})`,
        };
      }
    }
    // register inventory
    lastItem.description = product["name"] + (description ? description : "");
    lastItem.actualQty = lastItem.totalQty;
    lastItem.movedQty = quantity;
    // calculate total qty
    if (movementType.toLowerCase() === "ingreso") {
      currentQty += Number(quantity);
    } else {
      currentQty -= Number(quantity);
    }
    lastItem.totalQty = currentQty;
    lastItem.registerDate = currentDate.format();
    lastItem.movementType = movementType;
    lastItem.userId = "yo";
    lastItem.additionalInformation = additionalInformation;
    const infoLastItem = {
      ...lastItem,
    };
    delete infoLastItem._id;

    return await InventoryModel.create(infoLastItem);
  }

  async registerProductMovement(movement) {
    let secuence = 0;
    const lastMovement = await MovementModel.collection
      .find({
        movementType: movement.head.movementType,
      })
      .sort({
        creationDate: -1,
      })
      .limit(1)
      .toArray();
    if (lastMovement.length > 0) {
      secuence = Number(
        lastMovement[0].movementCode.replace(
          movement.head.movementType.substr(0, 3).toUpperCase() + "-",
          ""
        )
      );
    } else {
      secuence = 1;
    }
    secuence = secuence + 1;
    const numericSecuence =
      secuence < 10
        ? `00${secuence}`
        : secuence > 10 && secuence < 100
        ? `0${secuence}`
        : secuence;
    return MovementModel.create({
      movementType: movement.head.movementType,
      creationDate: new DateTime(movement.head.registerDate).format(),
      observations: movement.head.observations,
      provider: movement.head.provider || '',
      invoiceNumber: movement.head.invoiceNumber || '',
      userId: movement.userId,
      movementCode: `${movement.head.movementType
        .substr(0, 3)
        .toUpperCase()}-${numericSecuence}`,
    })
      .then(async (newMovement) => {
        for (const product of movement.details) {
          // get product information for a proper transformation of units
          const currentProduct = await ProductModel.collection.findOne({
            _id: mongoose.Types.ObjectId(product._id),
          });
          if (!("quantityPerBox" in currentProduct)) {
            currentProduct.quantityPerBox = 1;
          }

          const totalProductUnits =
            product.boxes * currentProduct.quantityPerBox + product.units;

          this.registerInventory(
            product._id,
            totalProductUnits,
            movement.head.movementType.toLowerCase(),
            ` Movimiento de productos (${newMovement._id})`,
            JSON.stringify(lastMovement)
          ).then(() => {
            MovementDetailModel.create({
              movementId: newMovement._id,
              productId: product._id,
              productName: product.text,
              boxes: product.boxes,
              units: product.units,
              actualPrice: 0,
              movementType: movement.head.movementType,
              lote: product.lote || '',
              laboratory: product.laboratory || '',
              expirationDate: product.expirationDate ? new DateTime(product.expirationDate).format() : '',
              provider: product.provider || '',
            });
          });
        }
        return { msg: "success" };
      })
      .catch((reason) => {
        return { msg: "error", message: reason.message };
      });
  }

  async getMinimunProducts() {
    const products = await ProductModel.collection
      .find()
      .sort({
        name: 1,
      })
      .toArray();
    const inventoriedProducts = products.map(async (product) => {
      // calculate actual inventory
      const inventory = await this.getInventoryInfo(product._id.toString());
      let actualQty = 0;
      if (inventory.length > 0) {
        actualQty = inventory[0].totalQty;
      }
      let boxes = 0;
      let unit_price = 0;
      if (!product.quantityPerBox) {
        product.quantityPerBox = 1;
      }
      if (product.quantityPerBox > 0) {
        boxes = Math.floor(actualQty / product.quantityPerBox);
        unit_price = product.price / product.quantityPerBox;
      }
      const units = actualQty - boxes * product.quantityPerBox;
      const productData = product;
      return await {
        ...productData,
        boxes,
        units,
        totalUnits: actualQty,
        unit_price: Number(unit_price),
      };
    });
    return Promise.all(inventoriedProducts);
  }

  async massiveInventorySave(products) {
    const totalQty = products.length;
    const inFilter = [];
    let qtyRegistered = {};
    for (let i = 0; i < totalQty; i++) {
      const cleanBarcode = String(products[i].barcode)
        .trim()
        .replace(/[^\w\s]/gi, "")
        .replace(/(?:\r\n|\r|\n)/g, "");
      const exists = await ProductModel.collection.findOne({
        barcode: cleanBarcode,
      });
      let currentProduct = undefined;
      if (exists) {
        currentProduct = {
          ...exists,
        };
      } else {
        const restrictedTypes = [
          "precursores",
          "psicotropicos",
          "estupefacientes",
        ];
        let restrictedType = "";
        let payIva = false;
        try {
          restrictedType =
            restrictedTypes.indexOf(
              products[i].restrictedType.trim().toLowerCase()
            ) >= 0
              ? products[i].restrictedType.trim().toLowerCase()
              : "medicamento";
        } catch (exTyMed) {
          restrictedType = "";
        }
        try {
          payIva =
            (products[i].payIva.trim().toLowerCase() === "si" ? true : false) ||
            false;
        } catch (exIva) {
          payIva = false;
        }
        currentProduct = await this.createProduct({
          barcode: cleanBarcode,
          name: products[i].name,
          description: "",
          prescription: "",
          dosis: "",
          quantityPerBox: products[i].unitsPerBox,
          laboratory: "",
          unitMinimumAlert: 0,
          restricted: false,
          price: products[i].price,
          priceDisscount: products[i].priceDisscount,
          restrictedType,
          payIva,
        });
      }
      const prodId = currentProduct._id.toString();
      inFilter.push(currentProduct._id);
      let actualQtyRegister = 0;
      if (isNaN(Number(qtyRegistered[prodId]))) {
        actualQtyRegister = Number(products[i].quantity);
      } else {
        actualQtyRegister =
          Number(qtyRegistered[prodId]) + Number(products[i].quantity);
      }
      qtyRegistered = {
        ...qtyRegistered,
        [prodId]: actualQtyRegister,
      };
    }

    const productsRegistered = await this.listProducts({
      multiple: inFilter,
    });
    const newProducts = productsRegistered.map((prod) => {
      return {
        ...prod,
        addedQuantity: qtyRegistered[prod._id],
      };
    });
    return newProducts;
  }

  getKardex(params) {
    let startDate = undefined;
    let endDate = undefined;
    if (params.endDate.toString() === "null") {
      delete params.endDate;
    } else {
      endDate = new DateTime(new Date(params.endDate).toISOString()).date.endOf(
        "day"
      );
    }
    if (params.startDate.toString() === "null") {
      delete params.startDate;
    } else {
      startDate = new DateTime(
        new Date(params.startDate).toISOString()
      ).date.startOf("day");
    }
    let filter: any = { productId: params.productId };
    if (startDate && !endDate) {
      filter = {
        ...filter,
        registerDate: {
          $gte: startDate.toDate(),
        },
      };
    }
    if (endDate && !startDate) {
      filter = {
        ...filter,
        registerDate: {
          $lte: endDate.toDate(),
        },
      };
    }
    if (startDate && endDate) {
      filter = {
        ...filter,
        registerDate: {
          $gte: startDate.toDate(),
          $lte: endDate.toDate(),
        },
      };
    }
    return InventoryModel.collection
      .find(filter)
      .sort({
        registerDate: -1,
      })
      .toArray();
  }

  async controlledProductsReport(params) {
    // calculate date for current values and past values
    const startDate = new DateTime(
      new Date(params.startDate).toISOString()
    ).date.startOf("day");
    const endDate = new DateTime(
      new Date(params.endDate).toISOString()
    ).date.endOf("day");
    const pastStartDate = startDate
      .clone()
      .subtract(1, "months")
      .startOf("month");
    const pastEndDate = startDate
      .clone()
      .subtract(1, "month")
      .endOf("month")
      .startOf("day");
    let filter: any = {};
    let pastFilter: any = {};
    if (startDate && endDate) {
      filter = {
        registerDate: {
          $gte: startDate.toDate(),
          $lte: endDate.toDate(),
        },
      };
      pastFilter = {
        registerDate: {
          $gte: pastStartDate.toDate(),
          $lte: pastEndDate.toDate(),
        },
      };
    }
    // get list or products with restriction
    const productsControlled = await ProductModel.collection
      .find({
        restricted: true,
      })
      .sort({
        name: 1,
      })
      .toArray();
    // get list of _ids to get inventory for all this products
    const products = productsControlled.map(async (product) => {
      // get past quantity for inventory for all products
      const pastInventory = await InventoryModel.collection
        .find({
          ...pastFilter,
          productId: product._id.toString(),
        })
        .sort({
          registerDate: -1,
        })
        .limit(1)
        .toArray();

      const actualInventory = await InventoryModel.collection
        .find({
          ...filter,
          productId: product._id.toString(),
        })
        .sort({
          registerDate: -1,
        })
        .limit(1)
        .toArray();
      const movements = await InventoryModel.collection
        .find({
          ...filter,
          productId: product._id.toString(),
        })
        .toArray();
      let incomes = 0;
      let outs = 0;
      movements.map((item) => {
        if (
          item["movementType"] === "egreso" ||
          item["movementType"] === "factura"
        ) {
          outs += Number(item["movedQty"]);
        }
        if (item["movementType"] === "ingreso") {
          incomes += Number(item["movedQty"]);
        }
      });
      const pastQty = pastInventory.length
        ? Number(pastInventory[0].totalQty)
        : 0;
      const calculated = pastQty + incomes - outs;

      return await {
        ...product,
        pastQty,
        final: actualInventory.length ? actualInventory[0].totalQty : 0,
        incomes,
        outs,
        calculated,
        movements,
      };
    });
    const pastInventory = await InventoryModel.collection.find();
    return Promise.all(products);
  }

  async generateXls(params) {
    const products = await this.listProducts({
      limit: 0,
      filter: params.filter,
    });
    const xls = new excel4node.Workbook();
    const hojaProductos = xls.addWorksheet("Productos");
    const productsQty = products.length;
    if (productsQty > 0) {
      let col = 1; // A
      let row = 2; // 2
      const keys = Object.keys(productColumns);
      const cellStyle = xls.createStyle({
        alignment: {
          wrapText: true,
        },
      });
      for (const key of keys) {
        hojaProductos.cell(1, col).string(productColumns[key]).style(cellStyle);
        col++;
      }
      col = 1;
      for (let i = 0; i < productsQty; i++) {
        col = 1;
        for (const key of keys) {
          let value = products[i][key] || "";
          if (value === "undefined") {
            value = "";
          }
          if (
            [
              "quantityPerBox",
              "unitMinimumAlert",
              "price",
              "priceDisscount",
            ].indexOf(key) >= 0
          ) {
            hojaProductos.cell(row, col).number(Number(value)).style(cellStyle);
          } else if (["payIva", "restricted"].indexOf(key) >= 0) {
            hojaProductos
              .cell(row, col)
              .string(value ? "SI" : "NO")
              .style(cellStyle);
          } else if (key === "restrictedType") {
            hojaProductos
              .cell(row, col)
              .string(value === "medicamento" ? "" : value.toLowerCase())
              .style(cellStyle);
          } else {
            hojaProductos
              .cell(row, col)
              .string(`${String(value)}`)
              .style(cellStyle);
          }
          col++;
        }
        row++;
      }
    }
    const path = `${__dirname}/../../../files/excelProductos.xlsx`;
    return new Promise((resolve, reject) => {
      const result = xls.write(path, (err, stats) => {
        if (err) {
          resolve({
            error: err.message,
          });
        } else {
          resolve({
            error: "",
            file: "excelProductos.xlsx",
          });
        }
      });
    });
  }

  async updateProductsBatch(products: any[]) {
    const productsQty = products.length;
    const columns = products[0];
    const keys = Object.keys(productColumns);
    for (let i = 1; i < productsQty; i++) {
      let product = {};
      let _id = "";
      for (const key of keys) {
        const col = keys.indexOf(key);
        if (key !== "_id") {
          if (key === "payIva" || key === "restricted") {
            if (products[i][col] && products[i][col].toLowerCase() === "si") {
              products[i][col] = true;
            } else {
              products[i][col] = false;
            }
          } else if (key === "restrictedType") {
            if (
              products[i][col] &&
              ["precursores", "psicotropicos", "estupefacientes"].indexOf(
                products[i][col].toLowerCase()
              ) >= 0
            ) {
              products[i][col] = products[i][col].toLowerCase();
            } else {
              products[i][col] = "medicamento";
            }
          }
          product = {
            ...product,
            [key]: products[i][col],
          };
        } else {
          _id = products[i][col];
        }
      }
      const updated = await this.updateProduct(_id, product);
    }
    return new Promise((resolve, response) => {
      resolve(products);
    });
  }

  async expiredProductsReport(params) {
    // const startDate = new DateTime(
    //   new Date(params.startDate).toISOString()
    // ).date.startOf("day");
    // const endDate = new DateTime(
    //   new Date(params.endDate).toISOString()
    // ).date.endOf("day");
    const { search } = params;
    const filter: any = {
      status: { $ne: 'ELI' },
    };
    if (search) {
      filter.name = { $regex: search, $options: 'i' }; // Busca coincidencias de manera insensible a mayúsculas/minúsculas
    }
    const productsControlled = await ProductModel.collection
      .find(filter)
      .sort({
        name: 1,
      })
      .toArray();
    const result = await Promise.all(productsControlled.map(async (product) => {
      const movementsDetails = await MovementDetailModel.collection
        .find({
          $and: [
            {productId: product._id.toString()},
            {movementType: 'ingreso'},
          ],
        }).toArray();
      const movementIds = movementsDetails.map(detail => detail.movementId);
      const movements = await MovementModel.collection
        .find({
          _id: { $in: movementIds.map(id => mongoose.Types.ObjectId(id)) }
        }).toArray();

      const detailedMovements = movementsDetails.map(detail => {
        const movement = movements.find(m => m._id.toString() === detail.movementId);
        return {
          ...detail,
          movement: movement || null
        };
      });
      return await {
        ...product,
        details: detailedMovements,
      };
    }));

    return result.filter(p => p.details.length > 0);
  }
}

const productsController = new ProductsController();
export { productsController as ProductsController };
