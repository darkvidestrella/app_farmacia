import { ClientModel } from '../models/models';
import * as mongoose from 'mongoose';

class ClientsController {
  async list(data) {
    let filters = {};
    if ('filter' in data) {
      filters = {
        $or: [
          { dni: new RegExp(data.filter, 'i') },
          { firstName: new RegExp(data.filter, 'i') },
          { lastName: new RegExp(data.filter, 'i') },
          { telephone: new RegExp(data.filter, 'i') },
          { email: new RegExp(data.filter, 'i') }
        ]
      };
    }
    return ClientModel.collection.find(filters).toArray();
  }

  async save(data) {
    const exists = await ClientModel.collection.findOne({dni: data.dni});
    if (exists) {
      return {
        msg: 'error',
        message: `El cliente ya existe [${data.dni}]`
      };
    }
    const client =  await ClientModel.create(data);
    if (client._id) {
      return {
        msg: 'success',
        data: client
      };
    } else {
      return {
        msg: 'error',
        message: client.errors
      };
    }
  }

  async update2(data) {
    const client = {
      ...data
    };
    delete client._id;
    const updated = await ClientModel.collection.findOneAndUpdate({
      _id: mongoose.Types.ObjectId(data._id)
    }, client);
    if (updated.ok) {
      return {
        msg: 'success',
        data: updated.value
      };
    } else {
      return {
        msg: 'error',
        data: 'El cliente no pudo ser actualizado'
      };
    }
  }
  async update(data) {
    const client = { ...data };
    delete client._id;

    try {
      const updated = await ClientModel.collection.findOneAndUpdate(
        { _id: mongoose.Types.ObjectId(data._id) },
        { $set: client },
        { returnOriginal: false },
      );

      if (updated.ok) {
        return {
          msg: 'success',
          data: updated.value
        };
      } else {
        return {
          msg: 'error',
          data: 'El cliente no pudo ser actualizado'
        };
      }
    } catch (error) {
      console.error('Error en la actualización del cliente:', error);
      return {
        msg: 'error',
        data: 'Ocurrió un error en la actualización del cliente'
      };
    }
  }

}

const clientsController = new ClientsController();
export { clientsController as ClientsController };
