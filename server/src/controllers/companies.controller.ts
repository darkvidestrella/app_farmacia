import { CompaniesModel } from '../models/models';

class CompaniesController {
  async listCompanies() {
    return await CompaniesModel.collection.find().toArray();
  }

  async createCompany(company) {
    const newCompany = await CompaniesModel.create(company);
    return newCompany;
  }

  async updateCompany(_id, company) {
    return CompaniesModel.findByIdAndUpdate({ _id }, company);
  }

  deleteCompany(_id: string) {
    return CompaniesModel.deleteOne({
      _id
    });
  }
}

const companiesController = new CompaniesController();
export { companiesController as CompaniesController };
