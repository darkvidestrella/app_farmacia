// express.d.ts
import * as express from 'express';

declare global {
    namespace Express {
        interface User {
            id: string;
            username: string;
        }
        export interface Request {
          tokenSec?: string;
        }
    }
}
