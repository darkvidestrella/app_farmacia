import * as moment from 'moment-timezone';

export enum Ambiente {
  Pruebas = 1,
  Produccion = 2,
}

export enum TipoEmision {
  EmisionNormal = 1,
}

export enum Comprobante {
  FACTURA = '01',
  LIQUIDACION_DE_COMPRA_DE_BIENES_Y_PRESTACION_DE_SERVICIOS = '03',
  NOTA_CREDITO = '04',
  NOTA_DEBITO = '05',
  GUIA_REMISION = '06',
  COMPROBANTE_RETENCION = '07',
}

export const razonSocial = () => {
  return 'ESTRELLA IZURIETA DAVID ERNESTO';
};

export const nombreComercial = () => {
  return 'FARMASALUD';
};

export const Ruc = () => {
  return '1716713035001';
};

export const SerieClaveAcceso = () => {
  return '002001';
};


export const claveAcceso = (secuencial) => {
  const fechaDeEmision = moment().format('DDMMYYYY');
  const codigoNumerico = new Date().valueOf().toString().split('').reverse().splice(0, 8).join('');
  const claveDeAcceso =
  fechaDeEmision + // paso 1
  Comprobante.FACTURA + // paso 2
  Ruc() + // paso 3
  Ambiente.Produccion + // paso 4
  SerieClaveAcceso() + // paso 5
  secuencial + // paso 6
  codigoNumerico + // paso 7
  TipoEmision.EmisionNormal + // paso 8
  digitoVerificador(codigoNumerico); // paso 9 digito verificador
  return claveDeAcceso;
};

export const estab = () => {
  return '002';
};

export const digitoVerificador = (numeroSecuencial) => {
  let multiplicador = 3;
  const limiteMultiplicador = 7;
  let total = 0;
  numeroSecuencial.toString().split('').forEach(numero => {
    const parcial = Number(numero) * multiplicador;
    total += parcial;
    multiplicador--;
    if (multiplicador < 2) {
      multiplicador = limiteMultiplicador;
    }
  });
  const digitoVerificado = 11 - total % 11;
  return `${digitoVerificado === 11 ? 0 : digitoVerificado === 10 ? 1 : digitoVerificado}`;
};
