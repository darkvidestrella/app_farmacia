import * as mongoose from 'mongoose';

class MongoModel {
  public schema: mongoose.Model<mongoose.Document>;
  constructor(name: string, schemaDefinition) {
    const PostSchema = new mongoose.Schema(schemaDefinition);
    this.schema = mongoose.model(name, PostSchema);
  }
}

/* Schemas definition */
// Companies
const CompaniesModel = new MongoModel('companies', {
  name: String,
  telephone: String,
  address: String
}).schema;

// Groups
const GroupModel = new MongoModel('groups', {
  name: String,
  menu: String,
  status: String
}).schema;

// Users
const UserModel = new MongoModel('users', {
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  group: String,
  status: String
}).schema;

// Products
const ProductModel = new MongoModel('products', {
  name: String,
  barcode: String,
  description: String,
  prescription: String,
  dosis: String,
  quantityPerBox: Number,
  laboratory: String,
  unitMinimumAlert: Number,
  restricted: Boolean,
  price: Number,
  priceDisscount: Number,
  restrictedType: String,
  payIva: Boolean,
  status: String
}).schema;

// Inventory
const InventoryModel = new MongoModel('inventory', {
  productId: String,
  description: String,
  actualQty: Number,
  movedQty: Number,
  totalQty: Number,
  registerDate: Date,
  movementType: String,
  userId: String,
  additionalInformation: String
}).schema;

const MovementModel = new MongoModel('movements', {
  movementType: String,
  creationDate: String,
  observations: String,
  itemRegistered: Number,
  movementCode: String,
  provider: String,
  invoiceNumber: String,
  userId: String
}).schema;

const MovementDetailModel = new MongoModel('movement_details', {
  movementId: String,
  productId: String,
  productName: String,
  boxes: Number,
  units: Number,
  actualPrice: Number,
  movementType: String,
  lote: String,
  laboratory: String,
  expirationDate: String,
  provider: String
}).schema;

// invoices
const ClientModel = new MongoModel('clients', {
  dni: String,
  firstName: String,
  lastName: String,
  address: String,
  telephone: String,
  email: String
}).schema;

const InvoiceModel = new MongoModel('invoices', {
  clientId: String,
  date: String,
  client: {
    dni: String,
    firstName: String,
    lastName: String,
    address: String,
    telephone: String,
    email: String
  },
  prescription: {
    dni: String,
    number: String,
    pacientName: String
  },
  disscountApplied: Boolean,
  products: Array,
  subtotal: Number,
  iva_pct: Number,
  iva: Number,
  disscount: Number,
  total: Number,
  status: String,
  invoiceNumber: String,
  userId: String
}).schema;

const InvoiceDetailsModel = new MongoModel('invoices_detail', {
  productId: String,
  barcode: String,
  boxes: Number,
  description: String,
  dosis: String,
  inventoryBoxes: Number,
  inventoryUnits: Number,
  laboratory: String,
  name: String,
  prescription: String,
  price: Number,
  quantityPerBox: Number,
  restricted: Boolean,
  selected: Boolean,
  subtotal: Number,
  totalUnits: Number,
  unitMinimumAlert: Number,
  unit_price: Number,
  units: Number
}).schema;

// laboratories
const LaboratoryModel = new MongoModel('laboratories', {
  name: String,
  address: String,
  telephone: String
}).schema;

// counter
const CounterModel = new MongoModel('Counters', {
  name: String, // Tipo: FACTURA, NOTA_CREDITO, ETC
  sequence: Number, // numero de secuencia que genera la factura en el SRI o para el SRI
}).schema;

// Logs
const LogModel = new MongoModel('Logs', {
  user: String,
  endpoint: String,
  method: String,
  statusCode: Number,
  timestamp: Date,
  requestData: Object
}).schema;

// final export
// ------------------------------------------------------------
// ------------------------------------------------------------
export {
  CompaniesModel,
  GroupModel,
  UserModel,
  ProductModel,
  InventoryModel,
  MovementModel,
  MovementDetailModel,
  ClientModel,
  InvoiceModel,
  InvoiceDetailsModel,
  LaboratoryModel,
  CounterModel,
  LogModel
};
