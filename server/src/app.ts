import * as express from 'express';
import * as expressSession from 'express-session';
import * as mongoose from 'mongoose';
import * as cors from 'cors';
import * as passport from 'passport';

import * as routes from './routes/router';
import * as apiRoutes from './routes/api-router';
import { CONFIG } from './configuration';
import logMiddleware from './middleware/log.middleware';

class Server {
  public app: express.Application;

  public static bootstrap(): Server {
    return new Server();
  }

  constructor() {
    this.app = express();
    this.app.set('port', 3001);

    this.app.use(express.json({ limit: '100mb' }));
    this.app.use(logMiddleware);
    this.app.use(express.urlencoded({ limit: '100mb' }));

    this.app.use(cors());

    this.app.use(expressSession({
      secret: 'pharmafarmasec',
      cookie: {}
    }))
    this.app.use(passport.initialize());
    this.app.use(passport.session());
    this.app.use(
      '/auth',
      routes
    );
    // secure path
    this.app.use('/api', apiRoutes);

    this.app.listen(this.app.get('port'), () => {
      this.startMongoConnection();
      console.log(
        'App is running at http://localhost:%d in %s mode bdd: ' + CONFIG.bdd,
        this.app.get('port'),
        this.app.get('env')
      );
      console.log('Press CTRL-C to stop\n');
    });
  }

  startMongoConnection() {
    console.log('Try connection to: ', `mongodb://${CONFIG.bdd}/farmacia_bdd`);
    mongoose.connect(`mongodb://${CONFIG.bdd}/farmacia_bdd`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
  }).then(() => {
      console.log('Mongo db connected');
    }).catch(() => {
      setTimeout(() => {
        this.startMongoConnection();
      }, 5000);
    });
  }

  lockApplication(req, res, next) {
    if (true) {
      next();
    } else {
      res.json({
        access: 'Unauthorized'
      }).end();
    }
  }
}

export = Server.bootstrap().app;
