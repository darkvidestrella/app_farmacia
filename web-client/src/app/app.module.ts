import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppMaterialModule } from './modules/material-design/app-material.module';

import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AjaxInterceptor } from './services/ajax.interceptor';

import { NavigationComponent } from './navigation/navigation.component';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfigurationComponent } from './modules/configuration/configuration.component';
import { CompaniesComponent } from './modules/configuration/companies/companies.component';
import { FormCompanyComponent } from './modules/configuration/companies/form-company/form-company.component';
import { ConfirmDialogComponent } from './shared/confirm-dialog/confirm-dialog.component';
import { ProductsComponent } from './modules/configuration/products/products.component';
import { InventoryComponent } from './modules/configuration/inventory/inventory.component';
import { ProductSelectorComponent } from './shared/product-selector/product-selector.component';
import { ProductListSelectorComponent } from './shared/product-selector/product-list-selector/product-list-selector.component';
import { NewProductFormComponent } from './modules/configuration/products/new-product-form/new-product-form.component';
import { InvoicesComponent } from './modules/invoices/invoices.component';
import { ClientSelectorComponent } from './shared/client-selector/client-selector.component';
import { ClientListSelectorComponent } from './shared/client-selector/client-list-selector/client-list-selector.component';
import { NewClientFormComponent } from './modules/clients/new-client-form/new-client-form.component';
import { ClientsComponent } from './modules/clients/clients.component';
import { TotalsBlockComponent } from './modules/invoices/totals-block/totals-block.component';
import { PrintModalComponent } from './modules/invoices/print-modal/print-modal.component';
import { InvoicesReportComponent } from './modules/invoices/invoices-report/invoices-report.component';
import { InventoryUploadComponent } from './modules/configuration/inventory/inventory-upload/inventory-upload.component';
import { EditProductFormComponent } from './modules/configuration/products/edit-product-form/edit-product-form.component';
import { InventoryListComponent } from './modules/configuration/inventory/inventory-list/inventory-list.component';
import {
  InventoryListDetailsComponent
} from './modules/configuration/inventory/inventory-list/inventory-list-details/inventory-list-details.component';
import { KardexComponent } from './modules/configuration/inventory/kardex/kardex.component';
import { ChangeCalculatorComponent } from './modules/invoices/change-calculator/change-calculator.component';
import { LoadingBarComponent } from './shared/loading-bar/loading-bar.component';
import { ControledProductsComponent } from './modules/reports/controled-products/controled-products.component';
import { LaboratoriesComponent } from './modules/configuration/laboratories/laboratories.component';
import { FormLaboratoryComponent } from './modules/configuration/laboratories/form-laboratory/form-laboratory.component';
import { InvoiceComponent } from './shared/templates/invoice/invoice.component';
import { PasswordModalModule } from './shared/password-modal/password-modal.module';
import { GroupsComponent } from './modules/configuration/groups/groups.component';
import { FormGroupComponent } from './modules/configuration/groups/form-group/form-group.component';
import { FormPermissionComponent } from './modules/configuration/groups/form-permission/form-permission.component';
import { UsersComponent } from './modules/configuration/users/users.component';
import { UsersFormComponent } from './modules/configuration/users/users-form/users-form.component';
import { PasswordChangeComponent } from './modules/configuration/users/password-change/password-change.component';
import { LoginComponent } from './modules/login/login.component';
import { AuthGuard } from './services/auth.guard';
import { HasPermissionDirective } from './services/has-permission.directive';
import { ProductDetailsModalComponent } from './modules/configuration/inventory/product-details-modal/product-details-modal.component';
import { ExpiredProductsComponent } from './modules/reports/expired-products/expired-products.component';
import { ReportsComponent } from './modules/reports/reports.component';
import { LogReportComponent } from './modules/reports/log-report/log-report.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'configuration',
    component: ConfigurationComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'companies',
        component: CompaniesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'laboratories',
        component: LaboratoriesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'products',
        component: ProductsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'inventory',
        component: InventoryComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'inventory/list',
        component: InventoryListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'invoices-report',
        component: InvoicesReportComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'kardex',
        component: KardexComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'controlados',
        component: ControledProductsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'expirados',
        component: ExpiredProductsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'groups',
        component: GroupsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'users',
        component: UsersComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'logs',
        component: LogReportComponent,
        canActivate: [AuthGuard],
      }
    ]
  },
  {
    path: 'invoice',
    component: InvoicesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'invoices-report',
    component: InvoicesReportComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    ConfigurationComponent,
    CompaniesComponent,
    FormCompanyComponent,
    ConfirmDialogComponent,
    ProductsComponent,
    InventoryComponent,
    ProductSelectorComponent,
    ProductListSelectorComponent,
    NewProductFormComponent,
    InvoicesComponent,
    ClientSelectorComponent,
    ClientListSelectorComponent,
    ClientsComponent,
    NewClientFormComponent,
    TotalsBlockComponent,
    PrintModalComponent,
    InvoicesReportComponent,
    InventoryUploadComponent,
    EditProductFormComponent,
    InventoryListComponent,
    InventoryListDetailsComponent,
    KardexComponent,
    ChangeCalculatorComponent,
    LoadingBarComponent,
    ControledProductsComponent,
    LaboratoriesComponent,
    FormLaboratoryComponent,
    InvoiceComponent,
    GroupsComponent,
    FormGroupComponent,
    FormPermissionComponent,
    UsersComponent,
    UsersFormComponent,
    PasswordChangeComponent,
    LoginComponent,
    HasPermissionDirective,
    ProductDetailsModalComponent,
    ExpiredProductsComponent,
    ReportsComponent,
    LogReportComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PasswordModalModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AjaxInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    FormCompanyComponent,
    ConfirmDialogComponent,
    ProductListSelectorComponent,
    NewProductFormComponent,
    ClientListSelectorComponent,
    NewClientFormComponent,
    PrintModalComponent,
    InventoryUploadComponent,
    EditProductFormComponent,
    InventoryListDetailsComponent,
    ChangeCalculatorComponent,
    FormLaboratoryComponent
  ]
})
export class AppModule {}
