import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { AjaxService } from './services/ajax.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AjaxService],
})
export class AppComponent implements OnInit {
  title = 'app';

  hasSession = false;

  constructor(
    private authService: AuthService,
    private ajax: AjaxService,
  ) {

  }

  ngOnInit(): void {
    this.ajax.get('/permissions').subscribe(result => {
      if (result.msg === 'success') {
        this.authService.setPermissions(result.data);
      }
      this.hasSession = this.authService.isAuthenticated();
    });
  }
}
