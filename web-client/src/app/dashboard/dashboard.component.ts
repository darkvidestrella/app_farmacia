import { Component, OnInit } from '@angular/core';
import { AngularBillboardService } from 'angular-billboard';
import { AjaxService } from '../services/ajax.service';
import { DateTime } from '../helpers/app.helpers';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [AjaxService]
})
export class DashboardComponent implements OnInit {
  cards = [
    { title: 'Card 1', cols: 2, rows: 1 },
    { title: 'Card 2', cols: 1, rows: 1 },
    { title: 'Card 3', cols: 1, rows: 2 },
    { title: 'Card 4', cols: 1, rows: 1 }
  ];

  chartDailySells: any[];
  chartsOptions: any[];
  productsMinimun: any[];
  dailyInvoicesData: any[];
  totalInvoiceSummary = 0;

  constructor(
    private angularBillboardService: AngularBillboardService,
    private ajax: AjaxService
  ) {}

  ngOnInit() {
    this.loadDailySells();
  }

  loadDailySells() {
    this.chartsOptions = [
      {
        data: {
          x: 'x',
          xFormat: '%Y-%m-%d %H:%M:%S',
          columns: [['x'], ['< $5'], ['entre $5 y $12'], ['> $12']],
          type: 'bubble',
          labels: false
        },
        bubble: {
          maxR: 20
        },
        axis: {
          x: {
            type: 'timeseries',
            tick: {
              format: '%H:%M'
            }
          }
        },
        title: {
          text: 'Ventas Diarias'
        }
      }
    ];
    this.chartDailySells = this.angularBillboardService.generate(
      ...this.chartsOptions
    );
    this.refreshDailySells();
    this.loadMinimunProducts();
    this.refreshDailyInvoices();
  }

  refreshDailyInvoices() {
    this.totalInvoiceSummary = 0;
    this.dailyInvoicesData = [];
    const date = DateTime().format();
    this.ajax
      .get('/invoices/day', {
        date
      })
      .subscribe(data => {
        if (data.msg === 'success') {
          this.dailyInvoicesData = data.data;
          this.dailyInvoicesData
            .forEach((item: any) => {
              if (item.status === 'emited') {
                this.totalInvoiceSummary += Number(item.total);
              }
            });
        }
      });
  }

  refreshDailySells() {
    const date = DateTime().format();
    this.ajax
      .get('/invoices/daily/sells', {
        date
      })
      .subscribe(data => {
        if (data.msg === 'success') {
          const invoicesData = data.data;
          this.dailyInvoicesData = invoicesData;
          const chartData: [any, any, any, any] = [
            ['x'],
            ['< $5'],
            ['entre $5 y $12'],
            ['> $12']
          ];
          invoicesData.forEach(val => {
            chartData[0].push(new Date(val.date));
            let low = 0;
            let medium = 0;
            let high = 0;
            if (val.value < 5) {
              low = val.value;
              medium = null;
              high = null;
            } else if (val.value >= 5 && val.value < 12) {
              low = null;
              medium = val.value;
              high = null;
            } else {
              low = null;
              medium = null;
              high = val.value;
            }
            chartData[1].push(low); // low
            chartData[2].push(medium); // medium
            chartData[3].push(high); // high
          });
          this.chartDailySells.map(chart => {
            chart.load({
              columns: chartData
            });
          });
        }
      });
  }

  loadMinimunProducts() {
    this.productsMinimun = [];
    this.ajax.get('/products/minimun').subscribe((data: any) => {
      if (data.data) {
        this.productsMinimun = data.data.filter(
          product => product.totalUnits <= product.unitMinimumAlert
        );
      }
    });
  }
}
