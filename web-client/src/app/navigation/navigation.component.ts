import { Component, OnInit } from '@angular/core';
import {
  BreakpointObserver,
  Breakpoints
} from '@angular/cdk/layout';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AjaxService } from '../services/ajax.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  providers: [AjaxService]
})
export class NavigationComponent {
  configuration: any = {
    hash: '',
    commit: ''
  };
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private breakpointObserver: BreakpointObserver,
    private ajax: AjaxService,
    private snackbar: MatSnackBar,
    private authService: AuthService,
  ) {
    this.getVersionInformation();
  }

  getVersionInformation() {
    this.ajax.get('/configuration').subscribe(response => {
      this.configuration = response;
      if (!('commit' in this.configuration)) {
        this.showConnectionError();
      } else if (this.configuration.commit === '') {
        this.showConnectionError();
      }
    });
  }

  showConnectionError() {
    this.snackbar.open('No hay conexión con el servidor', 'Cerrar');
  }

  logout() {
    this.authService.logout();
  }

}
