export interface Client {
  _id: string;
  dni: string;
  firstName: string;
  lastName: string;
  email?: string;
  telephone?: string;
  address?: string;
}
