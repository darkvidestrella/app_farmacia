export interface Laboratory {
  name: string;
  _id?: string;
  address?: string;
  telephone?: string;
}
