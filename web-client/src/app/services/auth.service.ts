import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() {}

  login(token: string, isAdmin?: boolean) {
    sessionStorage.setItem('authTokenPharma', token);
    if (isAdmin) {
      sessionStorage.setItem('authTokenPharmaAdmin', token);
    }
  }

  logout() {
    sessionStorage.removeItem('authTokenPharma');
    sessionStorage.removeItem('authTokenPharmaAdmin');
    sessionStorage.removeItem("pharma.serv.perms");
    window.location.href="/";
  }

  isAuthenticated(): boolean {
    return !!sessionStorage.getItem('authTokenPharma');
  }

  isAdmin(): boolean {
    return !!sessionStorage.getItem('authTokenPharmaAdmin');
  }

  getToken() {
    return sessionStorage.getItem('authTokenPharma');
  }

  setPermissions(permissions: any) {
    sessionStorage.setItem("pharma.serv.perms", JSON.stringify(permissions));
  }

  canActivate(permission: string) {
    const perms = JSON.parse(sessionStorage.getItem("pharma.serv.perms") || '[]');
    return perms?.length > 0 ? perms.includes(permission) : false;
  }
}
