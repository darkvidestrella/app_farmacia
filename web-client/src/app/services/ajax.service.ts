import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable()
export class AjaxService {

  private apiSegment = '/api';

  constructor(private http: HttpClient) {}

  get(url: string, data?: any, headers?: any): Observable<any> {
    return this.http
      .get(environment.serverUrl + this.apiSegment + url, {
        params: data,
        headers
      })
      .pipe(share());
  }

  post(url: string, data: any, headers?: any): Observable<any> {
    return this.http
      .post(environment.serverUrl + this.apiSegment + url, data, headers)
      .pipe(share());
  }

  put(url: string, data?: any, headers?: any): Observable<any> {
    return this.http
      .put(environment.serverUrl + this.apiSegment + url, {
        params: data,
        headers
      })
      .pipe(share());
  }

  delete(url: string, data?: any, headers?: any): Observable<any> {
    return this.http.delete(environment.serverUrl + this.apiSegment + url, {
      params: data,
      headers
    });
  }

  loadTpl(template: string) {
    return this.http.get(`../../assets/templates/${template}.txt`, {
      responseType: 'text'
    });
  }
}
