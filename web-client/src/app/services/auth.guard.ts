import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this.authService.isAuthenticated()) {
      if (this.authService.isAdmin()) {
        return true;
      }
      const mainRoutes = ['dashboard', 'invoice', 'configuration'];
      const mainRoute = state.url.split("/")[1];

      if (mainRoutes.includes(mainRoute)) {
        return this.authService.canActivate(mainRoute);
      }
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
