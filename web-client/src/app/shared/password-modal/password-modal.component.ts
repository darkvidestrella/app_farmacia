import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-password-modal',
  templateUrl: './password-modal.component.html',
  styleUrls: ['./password-modal.component.scss']
})
export class PasswordModalComponent implements OnInit {

  private MAIN_PASSWORD = btoa('Dario171622*');
  password = '';

  constructor(
    private dialogRef: MatDialogRef<PasswordModalComponent>
  ) { }

  ngOnInit(): void {
  }

  onEnter(event: any) {
    if (event.keyCode === 13) {
      this.validatePassword(true);
    }
  }

  validatePassword(action: boolean) {
    if (action) {
      if (this.password === atob(this.MAIN_PASSWORD)) {
        this.dialogRef.close({
          valid: true,
          message: '',
        });
      } else {
        this.dialogRef.close({
          valid: false,
          message: 'La clave ingresada no es correcta',
        });
      }
    } else {
      this.dialogRef.close({
        valid: false,
        message: '',
      });
    }
  }

}
