import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  title: string;
  text: string;
  cancelButton: boolean;

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.title = '';
    this.text = '';
  }

  ngOnInit() {
    this.title = this.data.title;
    this.text = this.data.text;
    this.cancelButton = this.data.cancelButton || false;
  }

  closeConfirm(action: boolean) {
    this.dialogRef.close(action);
  }
}
