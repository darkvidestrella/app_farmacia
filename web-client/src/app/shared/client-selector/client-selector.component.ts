import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ClientListSelectorComponent } from './client-list-selector/client-list-selector.component';

@Component({
  selector: 'app-client-selector',
  templateUrl: './client-selector.component.html',
  styleUrls: ['./client-selector.component.scss']
})
export class ClientSelectorComponent implements OnInit {

  @Input() searchEntryText: string;
  @Output() clientSelected = new EventEmitter();

  client = {
    searchText: '',
    firstName: '',
    lastName: '',
    address: '',
    telephone: '',
    _id: '',
    email: ''
  };

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    this.client.searchText = this.searchEntryText || '';
  }

  doClientSearch(event) {
    if (('key' in event) && event.key.toLowerCase() === 'enter') {
      this.clientSearch();
    }
  }

  clientSearch() {
    const dialog = this.dialog.open(ClientListSelectorComponent, {
      width: '80%',
      height: '80%',
      data: this.client.searchText || ''
    });
    dialog.afterClosed()
      .subscribe(result => {
        if (result) {
          this.client = {
            ...result
          };
          this.clientSelected.emit(this.client);
        }
      });
  }

}
