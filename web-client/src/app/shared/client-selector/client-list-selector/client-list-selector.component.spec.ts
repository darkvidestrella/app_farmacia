import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientListSelectorComponent } from './client-list-selector.component';

describe('ClientListSelectorComponent', () => {
  let component: ClientListSelectorComponent;
  let fixture: ComponentFixture<ClientListSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientListSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientListSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
