import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Client } from '../../../interface/client.interface';
import { AjaxService } from '../../../services/ajax.service';

@Component({
  selector: 'app-client-list-selector',
  templateUrl: './client-list-selector.component.html',
  styleUrls: ['./client-list-selector.component.scss'],
  providers: [AjaxService]
})
export class ClientListSelectorComponent implements OnInit {
  displayedColumns = ['dni', 'name', 'telephone'];
  clientsDataSource = new MatTableDataSource<Client[]>();
  searchInput: string;
  viewNewClientForm = false;
  selectedClient: string;
  editedClient: any = {};

  constructor(
    public dialogRef: MatDialogRef<ClientListSelectorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ajax: AjaxService
  ) {}

  ngOnInit() {
    this.selectedClient = '';
    if (this.data) {
      this.searchInput = this.data;
    }
    this.search();
  }

  search(selected?: string) {
    this.ajax
      .get('/clients', {
        filter: this.searchInput || ''
      })
      .subscribe(clients => {
        this.clientsDataSource.data = clients.data;
        if (selected) {
          const selectedClient = this.clientsDataSource.data.find(
            (client: any) => client._id === selected
          );
          this.selectClient(selectedClient);
        }
      });
  }

  toggleSelection(client) {
    if (this.selectedClient === '' || this.selectedClient !== client._id) {
      this.selectedClient = client._id;
    } else {
      this.selectedClient = '';
    }
  }

  doSearch(event) {
    if ('key' in event && event.key.toLowerCase() === 'enter') {
      this.search();
    }
  }

  newClient(clean = false) {
    this.viewNewClientForm = !this.viewNewClientForm;
    if (clean) {
      this.editedClient = {
        _id: '',
        firstName: '',
        lastName: '',
        dni: '',
        email: '',
        telephone: '',
        address: ''
      };
    }
  }

  cleanClientForm() {
    this.newClient();
  }

  closeSelectorModal() {
    this.dialogRef.close();
  }

  clientFormButtonClick(event) {
    if (event.action === 'cancel') {
      this.newClient();
    } else if (event.action === 'success') {
      this.searchInput = event.data.dni;
      this.search(event.data._id);
      this.newClient();
    }
  }

  selectClient(client) {
    this.selectedClient = client._id;
  }

  useClient(client) {
    this.selectClient(client);
    this.dialogRef.close(client);
  }

  editClient() {
    this.editedClient = this.clientsDataSource.data.find(
      (client: any) => client._id === this.selectedClient
    );
    this.newClient();
  }

  selectCurrentClient() {
    const currentClient = this.clientsDataSource.data.find((c: any) => c._id.toString() === this.selectedClient);
    this.dialogRef.close(currentClient);
  }
}
