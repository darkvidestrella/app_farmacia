import {
  Component,
  OnInit,
  Inject,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material/dialog";

import { MatTableDataSource } from "@angular/material/table";

import { AjaxService } from "../../../services/ajax.service";
import { ConfirmDialogComponent } from "../../confirm-dialog/confirm-dialog.component";

@Component({
  selector: "app-product-list-selector",
  templateUrl: "./product-list-selector.component.html",
  styleUrls: ["./product-list-selector.component.scss"],
  providers: [AjaxService],
})
export class ProductListSelectorComponent implements OnInit {
  productName: string;
  entryProduct: string;
  dataSource = new MatTableDataSource();
  displayedColumns = ["info", "name", "boxes", "units", "price", "unit_price"];
  selected = "";
  newProductForm = false;
  moveKeysActive = false;
  activeInformation = "";

  @ViewChild("inputSearch", { static: true }) inputSearch: ElementRef<
    HTMLInputElement
  >;
  @ViewChild("productSidenavRight", { static: true })
  productSidenavRight: ElementRef<HTMLElement>;
  productInformation: any = {};
  constructor(
    public dialogRef: MatDialogRef<ProductListSelectorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ajax: AjaxService,
    private dialog: MatDialog,
  ) {
    this.productName = "";
    this.entryProduct = "";
  }

  ngOnInit() {
    this.productName = this.data;
    this.searchProducts();
  }

  searchProducts() {
    this.selected = "";
    this.dataSource.data = [];
    if (this.productName.trim() !== "") {
      this.ajax
        .get("/products", {
          filter: this.productName.trim().toString(),
        })
        .subscribe((products) => {
          if (products.msg === "success") {
            this.dataSource.data = products.data;
          }
        });
    }
  }

  cleanSearch() {
    this.productName = "";
    this.dataSource.data = [];
    this.inputSearch.nativeElement.focus();
  }

  startSearch(event) {
    if ("key" in event) {
      if (event.key.toLowerCase() === "enter") {
        if (this.moveKeysActive === true) {
          this.moveKeysActive = false;
          this.selectMarkedProduct();
        } else {
          this.searchProducts();
        }
      }
      const arrowPressed = event.key.toLowerCase();
      if (arrowPressed === "arrowdown" || arrowPressed === "arrowup") {
        this.moveKeysActive = true;
        this.keyboardActions(arrowPressed);
      } else {
        this.moveKeysActive = false;
        this.selected = "";
      }
    }
  }

  keyboardActions(arrowPressed: string) {
    if (arrowPressed === "arrowdown" || arrowPressed === "arrowup") {
      if (this.dataSource.data.length > 0) {
        const lastIndex = this.dataSource.data.length - 1;
        let selectedIndex = 0;
        if (this.selected === "") {
          if (arrowPressed === "arrowdown") {
            this.selected = this.dataSource.data[0]["_id"];
            selectedIndex = -1;
          } else {
            this.selected = this.dataSource.data[lastIndex]["_id"];
            selectedIndex = lastIndex + 1;
          }
        } else {
          selectedIndex = this.dataSource.data.findIndex(
            (item: any) => item._id === this.selected
          );
        }
        if (arrowPressed === "arrowdown") {
          selectedIndex++;
          if (selectedIndex > lastIndex) {
            selectedIndex = 0;
          }
        } else {
          selectedIndex--;
          if (selectedIndex < 0) {
            selectedIndex = lastIndex;
          }
        }
        this.selected = this.dataSource.data[selectedIndex]["_id"];
      }
    }
  }

  selectMarkedProduct() {
    if (this.selected !== "") {
      const selected = this.dataSource.data.find(
        (item: any) => item._id === this.selected
      );
      this.selectProduct(selected);
    }
  }

  selectProduct(product) {
    const selectedProduct = { ...product };
    if (!selectedProduct.quantityPerBox) {
      selectedProduct.quantityPerBox = 1;
    }
    this.dialogRef.close(selectedProduct);
  }

  markProduct(id: string) {
    if (this.selected !== id) {
      this.selected = id;
    } else {
      this.selected = "";
    }
  }

  closeModal() {
    this.dialogRef.close();
  }

  createProduct() {
    this.newProductForm = !this.newProductForm;
    this.entryProduct = this.productName;
  }

  deleteProduct() {
    const selected = this.dataSource.data.find(
      (item: any) => item._id === this.selected
    );
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Eliminar producto",
        text: `Esto eliminará el producto ${selected["name"]} de sus registros, esta seguro que desea eliminar?`,
        cancelButton: true,
      },
    });
    dialog.afterClosed().subscribe((result) => {
      if (result === true) {
        this.ajax.delete("/products", { _id: this.selected }).subscribe(() => {
          this.searchProducts();
        });
      } else {
        this.selected = "";
      }
    });
  }

  processSavedNewProduct(event) {
    this.newProductForm = false;
    if (event.msg === "success") {
      this.productName = event.data.name;
      this.searchProducts();
    }
  }

  showProductInformation(product: any) {
    this.activeInformation = product._id;
    this.productSidenavRight.nativeElement.style.width = "calc(100% - 50px)";
    this.productSidenavRight.nativeElement.style.borderLeft = "solid 1px green";
    this.productInformation = {
      ...product,
    };
  }

  closeProductInformation() {
    this.productInformation = {};
    this.activeInformation = "";
    this.productSidenavRight.nativeElement.style.width = "0px";
    this.productSidenavRight.nativeElement.style.borderLeft =
      "solid 1px transparent";
  }
}
