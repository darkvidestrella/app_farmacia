import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ProductListSelectorComponent } from "./product-list-selector/product-list-selector.component";
import { Product } from "../../interface/product.interface";
import { environment } from "../../../environments/environment";
import { roundValue } from "src/app/helpers/app.helpers";

@Component({
  selector: "app-product-selector",
  templateUrl: "./product-selector.component.html",
  styleUrls: ["./product-selector.component.scss"],
})
export class ProductSelectorComponent implements OnInit, OnChanges {
  @Output() afterProductSelected = new EventEmitter();
  @ViewChild("productTextInput", { static: true })
  productTextInput: ElementRef<HTMLInputElement>;
  @ViewChild("boxField", { static: true }) boxField: ElementRef<
    HTMLInputElement
  >;
  @Input() editedProduct: any;
  @Input() restrictInventotyControl = false;
  @Input() hasDisscount: boolean;
  product: Product = {
    _id: "",
    name: "",
    companyId: "",
  };
  popupOpen = false;
  subtotalPreview = 0;

  constructor(private dialog: MatDialog, private snackbar: MatSnackBar) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes &&
      changes.editedProduct &&
      changes.editedProduct.currentValue
    ) {
      this.product = {
        ...changes.editedProduct.currentValue,
      };
    }
  }

  openFinder() {
    if (this.popupOpen === false) {
      this.popupOpen = true;
      const dialog = this.dialog.open(ProductListSelectorComponent, {
        width: "80%",
        height: "80%",
        data: String(this.product.name || "")
          .trim()
          .toString(),
      });
      dialog.afterClosed().subscribe((product: Product) => {
        this.popupOpen = false;
        let iva = 0;
        let iva_disscount = 0;
        if (product && "_id" in product) {
          if ("payIva" in product && product.payIva === true) {
            iva = (product.price * environment.IVA) / 100;
            iva_disscount =
              ((product.priceDisscount || product.price) * environment.IVA) /
              100;
          }
          this.product = {
            ...product,
            boxes: 0,
            units: 0,
            inventoryBoxes: product.boxes,
            inventoryUnits: product.units,
            iva,
            iva_disscount,
            quantityPerBox: product.quantityPerBox || 1,
          };
          this.boxField.nativeElement.focus();
        }
      });
    }
  }

  launchFinder(event) {
    this.product._id = "";
    this.product.boxes = 0;
    this.product.units = 0;
    if ("key" in event && event.key.toLowerCase() === "enter") {
      this.openFinder();
    }
  }

  previewSubtotalValue() {
    const totalEntered =
      this.product.boxes * this.product.quantityPerBox + this.product.units;
    if (this.hasDisscount === true) {
      this.subtotalPreview =
      roundValue(this.product.unit_price_disscount) * totalEntered;
    } else {
      this.subtotalPreview =
      roundValue(this.product.unit_price) * totalEntered;
    }
  }

  onlyNumbers(event) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      if ("key" in event && event.key.toLowerCase() === "enter") {
        this.addProduct();
      } else {
        event.preventDefault();
      }
    }
  }

  addProduct() {
    if (this.product._id) {
      if (this.product.boxes + this.product.units <= 0) {
        return;
      }
      const totalUnitsAvailable = this.product.totalUnits;
      const qtyInThisMovement =
        this.product.boxes * this.product.quantityPerBox + this.product.units;

      if (this.restrictInventotyControl) {
        if (
          qtyInThisMovement > totalUnitsAvailable ||
          totalUnitsAvailable <= 0
        ) {
          this.snackbar.open(
            `No tiene stock suficiente para este producto, solo hay ${totalUnitsAvailable} unidad(es) disponible(s)`
          );
          setTimeout(() => {
            this.snackbar.dismiss();
          }, 3000);
          return;
        }
      }
      // calculate subtotal per item
      const quantityOfUnits =
        this.product.quantityPerBox * this.product.boxes + this.product.units;
      const subtotal = quantityOfUnits * roundValue(this.product.unit_price);
      const subtotal_disscount =
        quantityOfUnits * roundValue(this.product.unit_price_disscount);
      const cachedProduct = {
        ...this.product,
        subtotal: Number(subtotal),
        subtotal_disscount: Number(subtotal_disscount),
        selected: true,
      };

      this.afterProductSelected.emit(cachedProduct);

      setTimeout(() => {
        this.product._id = "";
        this.product.name = "";
        this.product.boxes = 0;
        this.product.units = 0;
        this.product.inventoryBoxes = 0;
        this.product.inventoryUnits = 0;
        this.product.price = 0;
        this.product.unit_price = 0;
        this.product.unit_price_disscount = 0;
        this.product.quantityPerBox = 1;
        this.product.totalUnits = 0;
        this.product.unitsRegistered = qtyInThisMovement;
        this.productTextInput.nativeElement.focus();
        this.subtotalPreview = 0;
      }, 500);
    }
  }

  makePositive(key: string) {
    this.product[key] = Math.abs(this.product[key]);
  }
}
