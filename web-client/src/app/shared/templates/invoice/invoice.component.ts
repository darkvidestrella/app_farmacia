import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { DatePipe, CurrencyPipe } from "@angular/common";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: "app-invoice",
  templateUrl: "./invoice.component.html",
  styleUrls: ["./invoice.component.scss"],
  providers: [DatePipe, CurrencyPipe],
})
export class InvoiceComponent implements OnInit {
  @ViewChild("invoiceContent", { static: true }) invoiceContent: ElementRef<
    HTMLElement
  >;
  address: any;

  @Input() invoice: any;
  space = "                                       ";
  constructor(private datePipe: DatePipe, private currencyPipe: CurrencyPipe, private http: HttpClient) {}

  loadConfig() {
    return this.http.get('/assets/config-farma.json').subscribe((config: any) => {
      this.address = config?.direccion || 'SAN JOSE Y AV. CORDOVA GALARZA';
      this.buildInvoice();
    });
  }

  ngOnInit() {
    this.loadConfig();
  }

  buildInvoice() {
    let content = "";
    content += this.center("FARMASALUD");
    content += this.line(`DIRECCIÓN: ${this.address}`);
    content += this.center("QUITO - ECUADOR");
    content += this.line("RUC: 1716713035001");
    content += this.line(`FACTURA No: ${this.invoice.invoiceNumber}`);
    content += this.break();
    content += this.drawLine();
    content += this.tableLine("DESCRIPCIÓN.", "CANTID.", "P.V.U..", "SUBTOT.");
    for (let i = 0; i < this.invoice.products.length; i++) {
      const product = this.invoice.products[i];
      content += this.tableLine(
        product.name,
        `${product.boxes}c${product.units}u`,
        this.currencyPipe.transform(product.unit_price),
        this.currencyPipe.transform(product.subtotal)
      );
    }
    content += this.drawLine() + this.break();


    // subtotal
    content += this.totalLine(
      "SUBTOTAL:",
      this.currencyPipe.transform(this.invoice.subtotal + this.invoice.disscount)
    );
    content += this.totalLine(
      "DESCUENTO:",
      this.currencyPipe.transform(this.invoice.disscount)
    );
    if (this.invoice.disscount) {
      content += this.totalLine(
        "SUBTOTAL DESC:",
        this.currencyPipe.transform(this.invoice.subtotal )
      );
    }
    content += this.totalLine(
      `IVA (${this.invoice.iva_pct}%):`,
      this.currencyPipe.transform(this.invoice.iva)
    );
    content += this.totalLine(
      "TOTAL:",
      this.currencyPipe.transform(this.invoice.total)
    );
    content += this.break();
    content += this.drawLine();
    content += this.break();

    // client
    content += this.line(
      "CLIENTE: " +
        this.invoice.client.lastName +
        " " +
        this.invoice.client.firstName
    );
    content += this.line("CED/RUC: " + this.invoice.client.dni);
    content += this.line(
      "FEC EMI: " + this.datePipe.transform(this.invoice.date, "medium")
    );

    this.invoiceContent.nativeElement.innerText = content;
  }

  totalLine(text: string, value: string) {
    return text.padEnd(30, ".") + String(value).padStart(9, ".") + this.break();
  }

  line(text: string, spaces: number = 39, asArray: boolean = false) {
    const size = text.length;
    const lines = Math.ceil(size / spaces);
    let fullText = "";
    const lineArrays = [];
    let charCount = 0;
    for (let i = 0; i < lines; i++) {
      const str = text.substr(charCount, spaces);
      fullText += str + this.break();
      lineArrays.push(str + this.break());
      charCount += spaces;
    }
    if (asArray) {
      return lineArrays;
    } else {
      return fullText;
    }
  }

  break(breaks: number = 1) {
    let jump = "";
    for (let i = 0; i < breaks; i++) {
      jump += "\n";
    }
    return jump;
  }

  center(text: string) {
    const line = this.space.split("");
    const size = Math.floor(text.length / 2);
    const ini = Math.floor(line.length / 2) - size;
    for (let i = ini, j = 0; i < ini + text.length; i++, j++) {
      line[i] = text.substr(j, 1);
    }
    return line.join("\b") + this.break();
  }

  tableLine(
    description: string,
    quantity?: string,
    unitPrice?: string,
    subtotal?: string
  ) {
    const lineArr = this.line(description.substr(0, 24), 12, true);
    let lineTable = "";
    const separator = ".";
    for (let i = 0; i < lineArr.length; i++) {
      if (i === 0) {
        lineTable += lineArr[i].trim().padEnd(12, separator);
        lineTable += separator + separator;
        lineTable += quantity.padStart(5, separator);
        lineTable += separator + separator;
        lineTable += String(unitPrice).padStart(7, separator);
        lineTable += separator + separator;
        lineTable += String(subtotal).padStart(9, separator);
      } else {
        lineTable += lineArr[i];
      }
      lineTable += this.break();
    }
    return lineTable;
  }

  drawLine() {
    return "".padEnd(39, "-") + this.break();
  }
}
