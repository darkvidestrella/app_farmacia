// edit-lot-dialog.component.ts
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AjaxService } from 'src/app/services/ajax.service';

@Component({
  selector: 'app-edit-lot-dialog',
  templateUrl: "./product-details-modal.component.html",
  providers: [AjaxService],
})
export class ProductDetailsModalComponent implements OnInit{
  editLotForm: FormGroup;
  laboratories = [];

  constructor(
    public dialogRef: MatDialogRef<ProductDetailsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { product: any },
    private fb: FormBuilder,
    private ajax: AjaxService,
  ) {
    // Inicializar el formulario con valores vacíos
    this.editLotForm = this.fb.group({
      lote: [''],
      laboratory: [''],
      expirationDate: [''],
      provider: [''],
    });

    this.editLotForm.setValue({
      ...data,
    });
  }

  ngOnInit(): void {
    this.listLaboratories();
  }

  listLaboratories() {
    this.ajax.get('/laboratories').subscribe(result => {
      if (result.msg === 'success') {
        this.laboratories = result.data.map(laboratory => {
          return {
            name: laboratory.name,
            _id: laboratory.name
          };
        });
      }
    });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    if (this.editLotForm.valid) {
      this.dialogRef.close(this.editLotForm.value);
    }
  }
}
