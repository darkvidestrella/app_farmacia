import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { BehaviorSubject, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { AjaxService } from '../../../../services/ajax.service';
import { launchPrint } from 'src/app/helpers/app.helpers';

@Component({
  selector: 'app-kardex',
  templateUrl: './kardex.component.html',
  styleUrls: ['./kardex.component.scss'],
  providers: [AjaxService]
})
export class KardexComponent implements OnInit {
  productsStream$ = new BehaviorSubject([]);
  search = new FormControl();
  startDate = new FormControl();
  endDate = new FormControl();
  filteredAutocomplete: Observable<any[]>;
  kardexDataSource = new MatTableDataSource();
  selectedProduct = '';
  displayedColumns = ['item', 'description', 'creationDate', 'movementType', 'past', 'moved', 'final'];

  @ViewChild("reportContent", { static: true }) reportContent: ElementRef<
    HTMLElement
  >;

  constructor(private ajax: AjaxService) {}

  ngOnInit() {
    this.listProducts();
    this.filteredAutocomplete = this.search.valueChanges.pipe(
      startWith(''),
      map(
        prod =>
          prod ? this.filterProducts(prod) : this.productsStream$.value.slice()
      )
    );
  }

  filterProducts(value: any) {
    if (typeof value === 'string') {
      this.selectedProduct = '';
      const filterValue = value.toLowerCase();
      return this.productsStream$.value.filter(
        prod => prod.name.toLowerCase().indexOf(filterValue) === 0
      );
    }
  }

  listProducts() {
    this.ajax
      .get('/products', {
        limit: -1
      })
      .subscribe(result => {
        if (result.msg === 'success') {
          this.productsStream$.next(result.data);
        }
      });
  }

  startKardexView(event) {
    this.selectedProduct = event.option.value._id;
    this.searchKardex(this.selectedProduct);
  }

  printKardex(id: string) {
    launchPrint(
      "Kardex de Productos",
      this.reportContent.nativeElement.innerHTML
    );
  }

  searchKardex(id: string) {
    this.ajax
      .get('/inventory/kardex', {
        productId: id,
        startDate: this.startDate.value,
        endDate: this.endDate.value
      })
      .subscribe(result => {
        if (result.msg === 'success') {
          let item = 0;
          this.kardexDataSource.data = result.data.map(record => {
            item++;
            return {
              ...record,
              item
            };
          });
        } else {
          this.cleanKardex();
        }
      });
  }

  cleanKardex() {
    this.search.setValue('');
    this.kardexDataSource.data = [];
  }

  displayProductName(product?: any): string | undefined {
    return product ? product.name : undefined;
  }
}
