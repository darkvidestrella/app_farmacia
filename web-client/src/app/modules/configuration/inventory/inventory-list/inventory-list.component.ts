import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { DateTime, launchPrint } from '../../../../helpers/app.helpers';
import { AjaxService } from '../../../../services/ajax.service';
import { InventoryListDetailsComponent } from './inventory-list-details/inventory-list-details.component';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss'],
  providers: [AjaxService]
})
export class InventoryListComponent implements OnInit {
  @ViewChild("reportContent", { static: true }) reportContent: ElementRef<
    HTMLElement
  >;

  form: FormGroup;
  displayedColumns = [
    'item',
    'movementCode',
    'creationDate',
    'movementType',
    'provider',
    'invoiceNumber',
    'observation',
  ];
  movementsDataSource =  new MatTableDataSource();

  constructor(private fb: FormBuilder, private ajax: AjaxService, private dialog: MatDialog) {
    const startDate = DateTime();
    const endDate = DateTime();
    const movementType = 'ingreso,egreso';
    this.form = this.fb.group({
      startDate,
      endDate,
      movementType,
    });
  }

  ngOnInit() {}

  printReport() {
    launchPrint(
      "Listado de movimientos",
      this.reportContent.nativeElement.innerHTML
    );
  }

  searchMovements() {
    this.movementsDataSource.data = [];
    let item = 0;
    this.ajax
      .get('/inventory/movements/list', this.form.value)
      .subscribe(result => {
        this.movementsDataSource.data = result.data.map(movement => {
          item++;
          return {
            ...movement,
            item
          };
        });
      });
  }

  viewMovementDetails(id: string) {
    this.dialog.open(InventoryListDetailsComponent, {
      width: '90%',
      height: '90%',
      data: id
    });
  }
}
