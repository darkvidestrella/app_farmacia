import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryListDetailsComponent } from './inventory-list-details.component';

describe('InventoryListDetailsComponent', () => {
  let component: InventoryListDetailsComponent;
  let fixture: ComponentFixture<InventoryListDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryListDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryListDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
