import { Component, OnInit, Inject, ViewChild, ElementRef } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { AjaxService } from "../../../../../services/ajax.service";
import { launchPrint } from "src/app/helpers/app.helpers";

@Component({
  selector: "app-inventory-list-details",
  templateUrl: "./inventory-list-details.component.html",
  styleUrls: ["./inventory-list-details.component.scss"],
  providers: [AjaxService],
})
export class InventoryListDetailsComponent implements OnInit {
  detailsDataSource = new MatTableDataSource();
  movement: any = {};
  displayedColumns = ["item", "name", "boxes", "units", "lote", "provider", "laboratory", "expirationDate"];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild("reportContent", { static: true }) reportContent: ElementRef<
    HTMLElement
  >;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<InventoryListDetailsComponent>,
    private ajax: AjaxService
  ) {}

  ngOnInit() {
    this.getMovementDetail(this.data);
  }

  apllyFilter(text?: string) {
    try {
      this.detailsDataSource.filter = text.trim().toLowerCase();
    } catch (ex) {}
  }

  getMovementDetail(movementId: string) {
    let item = 0;
    this.ajax
      .get("/movement/details/list", {
        movementId,
      })
      .subscribe((result) => {
        this.movement = result.movement;
        this.detailsDataSource.data = result.data.map((product) => {
          item++;
          return {
            ...product,
            item,
            productName: product.product.name.toLowerCase(),
            productBarcode: product.product.barcode,
          };
        });
        this.detailsDataSource.sort = this.sort;
      });
  }

  printReport() {
    launchPrint(
      "Reporte de Movimiento",
      this.reportContent.nativeElement.innerHTML
    );
  }
}
