import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { DateTime } from '../../../helpers/app.helpers';
import { AjaxService } from '../../../services/ajax.service';
import { ConfirmDialogComponent } from '../../../shared/confirm-dialog/confirm-dialog.component';
import { Product } from '../../../interface/product.interface';
import { InventoryUploadComponent } from './inventory-upload/inventory-upload.component';
import { AuthService } from 'src/app/services/auth.service';
import { ProductDetailsModalComponent } from './product-details-modal/product-details-modal.component';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss'],
  providers: [AjaxService]
})
export class InventoryComponent implements OnInit {
  form: FormGroup;
  displayedColumns = ['details', '_id', 'name', 'boxes', 'units'];
  productsDataSource = new MatTableDataSource([]);
  editedProduct: any;
  buttonSaveClicked = false;
  loading = false;
  usePassword = false;

  constructor(
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    private ajax: AjaxService,
    private dialog: MatDialog,
    private authService: AuthService
  ) {
    this.editedProduct = {};
    const currentDate = DateTime();
    this.form = this.fb.group({
      movementType: '',
      creationDate: currentDate,
      observations: '',
      provider: '',
      invoiceNumber: '',
    });
  }

  ngOnInit() {
    const permissionEnabled = this.authService.canActivate('configuration.movements.register.income') || this.authService.canActivate('configuration.movements.register.outcome');
    this.buttonSaveClicked = !permissionEnabled;
  }

  newMovement() {
    const currentDate = DateTime();
    this.form.reset({ movementType: '', creationDate: currentDate, provier: '', invoiceNumber: '' });
    this.productsDataSource.data = [];
    this.buttonSaveClicked = false;
  }

  addProductItem(event) {
    const currentProducts = this.productsDataSource.data;
    const exists = currentProducts.find(item => item._id === event._id);
    if (exists) {
      const updatedProducts = currentProducts.map((item: Product) => {
        if (item._id === event._id) {
          // validate stock
          if (this.form.get('movementType').value.toLowerCase() === 'egreso') {
            const totalUnitsRegistered =
              (item.boxes + event.boxes) * item.quantityPerBox +
              (item.units + event.units);
            if (totalUnitsRegistered > item.totalUnits) {
              this.snackbar.open(
                `No tiene stock suficiente para seleccionar ${totalUnitsRegistered} unidad(es) (Disponible: ${
                  item.totalUnits
                })`,
                'Cerrar'
              );
              return exists;
            }
          }

          return {
            ...item,
            boxes: item.boxes + event.boxes,
            units: item.units + event.units,
            lote: '',
            laboratory: '',
            expirationDate: '',
            provider: '',
          };
        } else {
          return item;
        }
      });
      this.productsDataSource.data = updatedProducts;
    } else {
      currentProducts.push(event);
      this.productsDataSource.data = currentProducts;
    }
  }

  checkBeforeSave() {
    if (this.form.value.movementType === '') {
      this.snackbar.open('Seleccione el tipo de movimiento que va a realizar', 'Cerrar');
      this.buttonSaveClicked = false;
      return;
    }
    if ((this.authService.canActivate('configuration.movements.register.income') && this.form.value.movementType === 'ingreso') || (this.authService.canActivate('configuration.movements.register.outcome') && this.form.value.movementType === 'egreso')) {
      this.buttonSaveClicked = true;
      this.save();
    } else {
      this.buttonSaveClicked = false;
    }
    // if (this.usePassword) {
    //   const passwordDialog = this.dialog.open(PasswordModalComponent);
    //   passwordDialog.afterClosed().subscribe(result => {
    //     if (result.valid) {
    //       this.save();
    //     } else {
    //       if (result?.message !== '') {
    //         this.snackbar.open(result.message, 'Cerrar');
    //       }
    //       this.buttonSaveClicked = false;
    //     }
    //   });
    // } else {
    //   this.save();
    // }
  }

  save() {
    if (this.buttonSaveClicked) {
      if (this.productsDataSource.data.length <= 0) {
        this.snackbar.open('No se ha ingresado ningún producto', 'Cerrar');
        this.buttonSaveClicked = false;
        return;
      }
      this.loading = true;
      this.ajax
        .post('/movements', {
          head: {
            movementType: this.form.value.movementType,
            registerDate: this.form.value.creationDate.format(),
            observations: this.form.value.observations,
            provider: this.form.value.provider,
            invoiceNumber: this.form.value.invoiceNumber,
          },
          details: this.productsDataSource.data
        })
        .subscribe(result => {
          this.loading = false;
          if (result.msg === 'success') {
            const dialog = this.dialog.open(ConfirmDialogComponent, {
              data: {
                title: 'Guardado exitosamente',
                text:
                  'El movimiento de productos fue registrado exitosamente',
                  cancelButton: false
              }
            });
            dialog.afterClosed().subscribe(() => {
              this.newMovement();
            });
          } else {
            this.snackbar.open(result.message, 'Cerrar');
          }
          this.buttonSaveClicked = false;
        }, (err) => {
          this.snackbar.open(err.message, 'Cerrar');
          this.buttonSaveClicked = false;
          this.loading = false;
        });
    }
  }

  editCurrentProduct(product) {
    const clickedProduct = this.productsDataSource.data.find(
      item => item._id === product._id
    );
    const newList = this.productsDataSource.data.filter(
      item => item._id !== product._id
    );
    this.productsDataSource.data = newList;
    this.editedProduct = clickedProduct;
  }

  removeProduct(id: string) {
    const newList = this.productsDataSource.data
      .filter(item => item._id !== id);
    this.productsDataSource.data = newList;
  }

  excelUpload() {
    const dialog = this.dialog.open(InventoryUploadComponent, {
      width: '80%',
      height: '80%',
      data: {}
    });
    dialog.afterClosed().subscribe(result => {
      this.loading = true;
      this.ajax
        .post('/inventory/fast-inventory-registration', result)
        .subscribe(response => {
          if (response.msg === 'success') {
            response.data.forEach(item => {
              const newItem = { ...item, units: 0, boxes: item.addedQuantity };
              this.addProductItem(newItem);
            });
          } else {
            this.snackbar.open(response.message, 'Cerrar');
          }
          this.loading = false;
        }, () => {
          this.loading = false;
        });
    });
  }

  checkPassword() {
    // this.usePassword = this.form.get('movementType').value.toLowerCase() === 'egreso';
  }

  updateProductDetails(product: any) {
    const productId = product._id.toString();
    if (!productId) return;
    const dialog = this.dialog.open(ProductDetailsModalComponent, {
      data: {
        lote: product.lote || '',
        expirationDate: product.expirationDate || '',
        provider: product.provider || '',
        laboratory: product.laboratory || '',
      },
    });
    dialog.afterClosed().subscribe(data => {
      if (data) {
        const currentProducts = this.productsDataSource.data;
        const exists = currentProducts.find(item => item._id === productId);
        if (exists) {
          const updatedProducts = currentProducts.map((item: Product) => {
            if (item._id === productId) {
              return {
                ...item,
                ...data,
              };
            } else {
              return item;
            }
          });
          this.productsDataSource.data = updatedProducts;
        }
      }
    });
  }
}
