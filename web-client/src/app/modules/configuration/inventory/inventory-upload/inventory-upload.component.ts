import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  Inject,
} from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatTableDataSource } from "@angular/material/table";
import { AjaxService } from "../../../../services/ajax.service";

@Component({
  selector: "app-inventory-upload",
  templateUrl: "./inventory-upload.component.html",
  styleUrls: ["./inventory-upload.component.scss"],
  providers: [AjaxService],
})
export class InventoryUploadComponent implements OnInit, AfterViewInit {
  uploadForm: FormGroup;
  productsDataSource = new MatTableDataSource();

  @ViewChild("fileButton", { static: true }) fileButton: ElementRef<
    HTMLButtonElement
  >;
  @ViewChild("submitButton", { static: true }) submitButton: ElementRef<
    HTMLButtonElement
  >;

  columnsFound = [];
  xlsData = [];
  displayedColumns = [
    "barcode",
    "name",
    "price",
    "priceDisscount",
    "unitsPerBox",
    "quantity",
    "payIva",
    "restrictedType",
  ];
  columnTitles = {
    barcode: "Codigo barras",
    name: "Producto",
    price: "PVP",
    unitsPerBox: "Unidades por caja",
    quantity: "Cajas ingresadas",
    payIva: "Registra IVA SI/NO",
    priceDisscount: "Precio con Descuento",
    restrictedType: "Tipo / Controlado",
  };
  columnRelations = {
    barcode: "",
    name: "",
    price: "",
    priceDisscount: "",
    unitsPerBox: "",
    quantity: "",
    payIva: "",
    restrictedType: "",
  };
  processButtonDisabled = true;

  constructor(
    private fb: FormBuilder,
    private ajax: AjaxService,
    private snackbar: MatSnackBar,
    public dialogRef: MatDialogRef<InventoryUploadComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.uploadForm = this.fb.group({ file: [null] });
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.fileButton.nativeElement.click();
  }

  uploadFileToServer() {
    this.xlsData = [];
    this.columnRelations = {
      barcode: "",
      name: "",
      price: "",
      unitsPerBox: "",
      quantity: "",
      priceDisscount: "",
      payIva: "",
      restrictedType: "",
    };
    const formData: FormData = new FormData();
    formData.append(
      "file",
      this.uploadForm.value.file,
      this.uploadForm.value.file.name
    );
    this.ajax.post("/inventory/upload-file", formData).subscribe((result) => {
      if (result.msg === "success") {
        this.buildViewForLoadItems(result.data);
      } else {
        this.snackbar.open(result.message, "Cerrar");
      }
    });
  }

  checkFileSelected(event) {
    if (event.target.files.length > 0) {
      const [file] = event.target.files;
      this.uploadForm.patchValue({
        file,
      });
      this.submitButton.nativeElement.click();
      setTimeout(() => {
        this.uploadForm.patchValue({
          fil: null,
        });
      }, 1000);
    }
  }

  buildViewForLoadItems(xlsx: any) {
    if (xlsx[0].data.length > 0) {
      const hoja = xlsx[0].data;
      this.columnsFound = hoja[0];
      this.xlsData = hoja.splice(1, hoja.length);
    } else {
      this.snackbar.open(
        `No se encontraron datos en la hoja ${xlsx[0].name}`,
        "Cerrar"
      );
    }
  }

  productsDataWasLoaded() {
    let enable = 0;
    Object.values(this.columnRelations).map((value) => {
      if (!value) {
        enable++;
      }
    });
    if (enable === 0) {
      return true;
    } else {
      return false;
    }
  }

  refreshDataLoaded(event, column: string) {
    const index = this.columnsFound.findIndex((item) => item === event.value);
    this.columnRelations[column] = index;
    const enable = this.productsDataWasLoaded();
    this.processButtonDisabled = true;
    if (enable) {
      this.processButtonDisabled = false;
      this.processData();
    }
  }

  processData() {
    const enable = this.productsDataWasLoaded();
    if (enable === false) {
      return;
    }
    this.productsDataSource.data = this.xlsData.map((product) => {
      return {
        barcode: product[this.columnRelations.barcode],
        name: product[this.columnRelations.name],
        price: product[this.columnRelations.price],
        unitsPerBox: product[this.columnRelations.unitsPerBox],
        quantity: product[this.columnRelations.quantity],
        payIva: product[this.columnRelations.payIva],
        priceDisscount: product[this.columnRelations.priceDisscount],
        restrictedType: product[this.columnRelations.restrictedType],
      };
    });
  }

  retreiveData() {
    this.dialogRef.close(this.productsDataSource.data);
  }
}
