import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AjaxService } from 'src/app/services/ajax.service';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.scss'],
  providers: [AjaxService],
})
export class PasswordChangeComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<PasswordChangeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ajax: AjaxService,
    private fb: FormBuilder,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      _id: '',
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    });
  }

  closeForm() {
    this.form.reset();
    this.dialogRef.close();
  }

  save() {
    if (this.form.valid) {
      if (this.form.value._id && this.form.value._id !== '') {
        if (this.form.value.password !== this.form.value.confirmPassword) {
          this.snackbar.open('Las claves ingresadas deben ser iguales', 'Cerrar');
          return;
        }
        const userInfo = {
          _id: this.form.value._id,
          password: this.form.value.password,
        };
        this.ajax.put('/users', userInfo).subscribe(result => {
          if (result.msg === 'success') {
            this.dialogRef.close(true);
          } else {
            this.snackbar.open(result.message, 'Cerrar');
          }
        });
      } else {
        this.snackbar.open('Hubo un error al seleccionar el usuario', 'Cerrar');
      }
    } else {
      this.snackbar.open('El formulario tiene errores', 'Cerrar');
    }
  }

  ngOnInit() {
    if (this.data) {
      this.form.setValue({
        _id: this.data._id,
        password: '',
        confirmPassword: '',
      });
    }
  }

}
