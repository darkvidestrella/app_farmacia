import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { AjaxService } from 'src/app/services/ajax.service';
import { FormGroupComponent } from '../groups/form-group/form-group.component';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { UsersFormComponent } from './users-form/users-form.component';
import { PasswordChangeComponent } from './password-change/password-change.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  form: FormGroup;
  usersDataSource = new MatTableDataSource();
  displayedColumns = ["firstName", "lastName", "email", "group", "_id"];
  checkExistence = false;
  runFirstLoad = false;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild("filterText", { static: true }) filterText: ElementRef<
    HTMLInputElement
  >;

  groups: any[] = [];

  constructor(
    private fb: FormBuilder,
    private ajax: AjaxService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      name: ["", Validators.required],
    });
  }

  listGroups() {
    this.ajax.get('/groups').subscribe(groups => {
      this.groups = groups?.data || [];
      this.list();
    })
  }

  list() {
    const element = this.filterText.nativeElement;
    if (element) {
      this.usersDataSource.data = [];
      this.ajax
        .get("/users", {
          filter: element.value || "",
        })
        .subscribe((response) => {
          if (response.msg === "success") {
            this.usersDataSource.data = response.data.map(user => {
              return {
                ...user,
                groupName: this.groups.find(g => g._id.toString() === user.group)?.name || '',
              };
            });
            this.usersDataSource.paginator = this.paginator;
          }
        });
    }
  }

  delete(_id: string) {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Eliminar usuario?",
        text: "Esta seguro que desea eliminar este usuario, al eliminar ya no tendran acceso",
        cancelButton: true,
      },
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.ajax
          .delete("/users", {
            _id,
          })
          .subscribe((response) => {
            if (response.msg === "success") {
              this.list();
            } else {
              this.snackbar.open(response.message, "Cerrar");
            }
          });
      }
    });
  }

  openForm(user: any) {
    const dialog = this.dialog.open(UsersFormComponent, {
      data: user || {},
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.list();
      }
    });
  }

  openChangePasswordForm(user: any) {
    if (!user) return
    const dialog = this.dialog.open(PasswordChangeComponent, {
      data: user,
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.snackbar.open("La clave fue actualizada correctamente", "Cerrar");
        this.list();
      }
    });
  }

  applyFilter() {
    this.list();
  }

  ngOnInit() {
    this.listGroups();
  }

}
