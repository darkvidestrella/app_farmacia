import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AjaxService } from 'src/app/services/ajax.service';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss'],
  providers: [AjaxService],
})
export class UsersFormComponent implements OnInit {
  form: FormGroup;
  groupsList = []

  constructor(
    public dialogRef: MatDialogRef<UsersFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ajax: AjaxService,
    private fb: FormBuilder,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      _id: '',
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      password: '',
      group: ['', Validators.required],
    });
  }

  listGroups() {
    this.ajax.get("/groups").subscribe(response => {
      if (response.msg === 'success') {
        this.groupsList = response.data;
      } else {
        this.groupsList = [];
      }
    });
  }

  closeForm() {
    this.form.reset();
    this.dialogRef.close();
  }

  save() {
    if (this.form.valid) {
      if (this.form.value._id && this.form.value._id !== '') {
        const userInfo = {
          _id: this.form.value._id,
          firstName: this.form.value.firstName,
          lastName: this.form.value.lastName,
          email: this.form.value.email,
          group: this.form.value.group
        };
        this.ajax.put('/users', userInfo).subscribe(result => {
          if (result.msg === 'success') {
            this.dialogRef.close(true);
          } else {
            this.snackbar.open(result.message, 'Cerrar');
          }
        });
      } else {
        const userInfo = {
          firstName: this.form.value.firstName,
          lastName: this.form.value.lastName,
          email: this.form.value.email,
          group: this.form.value.group
        };
        this.ajax.post('/users', userInfo).subscribe(result => {
          if (result.msg === 'success') {
            this.dialogRef.close(true);
          } else {
            this.snackbar.open(result.message, 'Cerrar');
          }
        });
      }
    } else {
      this.snackbar.open('El formulario tiene errores', 'Cerrar');
    }
  }

  onGroupChange(event: any) {
    this.form.controls["group"].setValue(event.value || "");
  }

  ngOnInit() {
    this.listGroups();
    if (this.data) {
      this.form.setValue({
        _id: this.data['_id'] || '',
        firstName: this.data['firstName'] || '',
        lastName: this.data['lastName'] || '',
        email: this.data['email'] || '',
        group: this.data['group'] || '',
        password: '',
      });
    }
  }

}
