import { Component, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AjaxService } from '../../../../services/ajax.service';

@Component({
  selector: 'app-form-company',
  templateUrl: './form-company.component.html',
  styleUrls: ['./form-company.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [AjaxService]
})
export class FormCompanyComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<FormCompanyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private ajax: AjaxService,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      _id: '',
      name: ['', Validators.required],
      address: '',
      telephone: ''
    });
    this.form.setValue(this.data.company);
  }

  saveCompany() {
    if (this.data.company._id !== '') {
      this.ajax
        .put('/companies', this.form.value)
        .subscribe(
          data => this.dialogRef.close(data),
          error => {
            console.log(error);
          }
        );
    } else {
      this.ajax
        .post('/companies', this.form.value)
        .subscribe(
          data => {
            if (data.msg === 'success') {
              this.dialogRef.close(data);
            } else {
              this.snackbar.open(data.message, 'Cerrar');
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  closeForm() {
    this.dialogRef.close();
  }
}
