import { Component, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatTableDataSource } from "@angular/material/table";

import { BehaviorSubject } from "rxjs";

import { AjaxService } from "../../../services/ajax.service";
import { FormCompanyComponent } from "./form-company/form-company.component";
import { Company } from "../../../interface/company.interface";
import { ConfirmDialogComponent } from "../../../shared/confirm-dialog/confirm-dialog.component";

@Component({
  selector: "app-companies",
  templateUrl: "./companies.component.html",
  styleUrls: ["./companies.component.scss"],
  providers: [AjaxService],
})
export class CompaniesComponent implements AfterViewInit {
  displayedColumns = ["name", "address", "telephone", "_id"];
  companiesData$ = new BehaviorSubject([]);
  companyDataSource: MatTableDataSource<Company> = new MatTableDataSource([]);
  @ViewChild("filter", { static: true }) filter: ElementRef<HTMLInputElement>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private ajax: AjaxService,
    private form: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.companiesData$.subscribe((companies: Company[]) => {
      this.companyDataSource.data = companies;
    });
  }

  applyFilter(filterText?: string) {
    this.companyDataSource.filter = filterText.trim().toLowerCase();
  }

  ngAfterViewInit() {
    this.listCompanies();
    this.companyDataSource.paginator = this.paginator;
  }

  listCompanies() {
    this.ajax.get("/companies").subscribe(
      (companies) => {
        this.companiesData$.next(companies.data);
      },
      (error) => console.log(error.message)
    );
  }

  openForm(_id: string, currentCompany?: Company) {
    let company: Company = {
      _id: "",
      name: "",
      address: "",
      telephone: "",
    };
    if (_id !== "") {
      company = {
        _id: currentCompany._id,
        name: currentCompany.name,
        address: currentCompany.address,
        telephone: currentCompany.telephone,
      };
    }
    const dialog = this.form.open(FormCompanyComponent, {
      data: {
        company,
      },
    });
    dialog.afterClosed().subscribe((data) => {
      this.listCompanies();
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  deleteCompany(_id: string) {
    const dialog = this.form.open(ConfirmDialogComponent, {
      disableClose: true,
      data: {
        title: "Eliminar",
        text: "Confirme que desea eliminar?",
      },
    });
    dialog.afterClosed().subscribe((result) => {
      if (result === true) {
        this.ajax.delete("/companies", { _id }).subscribe(() => {
          this.listCompanies();
        });
      }
    });
  }
}
