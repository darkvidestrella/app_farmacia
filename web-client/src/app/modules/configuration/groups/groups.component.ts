import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { AjaxService } from 'src/app/services/ajax.service';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { FormGroupComponent } from './form-group/form-group.component';
import { FormPermissionComponent } from './form-permission/form-permission.component';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  form: FormGroup;
  groupsDataSource = new MatTableDataSource();
  displayedColumns = ["name", "_id"];
  checkExistence = false;
  runFirstLoad = false;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild("filterText", { static: true }) filterText: ElementRef<
    HTMLInputElement
  >;

  constructor(
    private fb: FormBuilder,
    private ajax: AjaxService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      name: ["", Validators.required],
      menu: "",
    });
  }

  list() {
    const element = this.filterText.nativeElement;
    if (element) {
      this.groupsDataSource.data = [];
      this.ajax
        .get("/groups", {
          filter: element.value || "",
        })
        .subscribe((response) => {
          if (response.msg === "success") {
            this.groupsDataSource.data = response.data;
            this.groupsDataSource.paginator = this.paginator;
          }
        });
    }
  }

  delete(_id: string) {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Eliminar grupo?",
        text: "Esta seguro que desea eliminar este grupo, los usuarios asociados a este se eliminaran y no tendran acceso",
        cancelButton: true,
      },
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.ajax
          .delete("/groups", {
            _id,
          })
          .subscribe((response) => {
            if (response.msg === "success") {
              this.list();
            } else {
              this.snackbar.open(response.message, "Cerrar");
            }
          });
      }
    });
  }

  openForm(group: any) {
    const dialog = this.dialog.open(FormGroupComponent, {
      data: group || {},
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.list();
      }
    });
  }

  openPermissionForm(group: any) {
    const dialog = this.dialog.open(FormPermissionComponent, {
      data: group || {},
      width: '80%',
      height: '80%',
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.list();
      }
    });
  }

  applyFilter() {
    this.list();
  }

  ngOnInit() {
    this.list();
  }

}
