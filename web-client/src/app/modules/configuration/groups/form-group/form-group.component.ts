import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AjaxService } from 'src/app/services/ajax.service';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss'],
  providers: [AjaxService]
})
export class FormGroupComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<FormGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ajax: AjaxService,
    private fb: FormBuilder,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      _id: '',
      name: ['', Validators.required],
      menu: ''
    });
  }

  closeForm() {
    this.form.reset();
    this.dialogRef.close();
  }

  save() {
    if (this.form.valid) {
      if (this.form.value._id && this.form.value._id !== '') {
        this.ajax.put('/groups', this.form.value).subscribe(result => {
          if (result.msg === 'success') {
            this.dialogRef.close(true);
          } else {
            this.snackbar.open(result.message, 'Cerrar');
          }
        });
      } else {
        const groupInfo = {
          name: this.form.value.name,
          menu: this.form.value.menu
        };
        this.ajax.post('/groups', groupInfo).subscribe(result => {
          if (result.msg === 'success') {
            this.dialogRef.close(true);
          } else {
            this.snackbar.open(result.message, 'Cerrar');
          }
        });
      }
    } else {
      this.snackbar.open('El formulario tiene errores', 'Cerrar');
    }
  }

  ngOnInit() {
    if (this.data) {
      this.form.setValue({
        _id: this.data['_id'] || '',
        name: this.data['name'] || '',
        menu: this.data['address'] || ''
      });
    }
  }

}
