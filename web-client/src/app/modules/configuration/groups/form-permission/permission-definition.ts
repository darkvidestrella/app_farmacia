export interface PermissionNode {
  name: string;
  key?: string;
  children?: PermissionNode[];
}

export const TREE_DATA: PermissionNode[] = [
  {
    name: 'Dashboard',
    key: "dashboard",
  },
  {
    name: 'Configuration',
    key: "configuration",
    children: [
      {
        name: 'Icono Dashboard',
        key: 'configuration.dashboard',
      },
      {
        name: 'General',
        key: 'configuration.general',
        children: [
          {
            name: 'Laboratorios',
            key: 'configuration.general.laboratories',
            children: [
              {
                name: 'Agregar laboratorio',
                key: 'configuration.general.laboratories.add'
              },
              {
                name: 'Editar laboratorio',
                key: 'configuration.general.laboratories.edit'
              },
              {
                name: 'Borrar laboratorio',
                key: 'configuration.general.laboratories.delete'
              },
            ],
          },
          {
            name: 'Productos',
            key: 'configuration.general.products',
            children: [
              {
                name: 'Agregar producto',
                key: 'configuration.general.products.add'
              },
              {
                name: 'Editar producto',
                key: 'configuration.general.products.edit'
              },
              {
                name: 'Eliminar producto',
                key: 'configuration.general.products.delete'
              },
            ],
          },
        ],
      },
      {
        name: 'Movimientos',
        key: 'configuration.movements',
        children: [
          {
            name: 'Lista de movimientos',
            key: 'configuration.movements.list',
          },
          {
            name: 'Registrar movimiento',
            key: 'configuration.movements.register',
            children: [
              {
                name: 'Guardar ingreso',
                key: 'configuration.movements.register.income'
              },
              {
                name: 'Guardar egreso',
                key: 'configuration.movements.register.outcome'
              },
              {
                name: 'Cargar desde excel',
                key: 'configuration.movements.register.upload'
              },
            ],
          },
        ],
      },
      {
        name: 'Reportes',
        key: 'configuration.reports',
        children: [
          {
            name: 'Reporte de facturas',
            key: 'configuration.reports.invoices',
          },
          {
            name: 'Kardex de productos',
            key: 'configuration.reports.kardex',
          },
          {
            name: 'Productos controlados',
            key: 'configuration.reports.controlled',
          },
          {
            name: 'Reporte de caducidades',
            key: 'configuration.reports.expired',
          },
        ],
      },
      {
        name: 'Seguridad',
        key: 'configuration.users',
        children: [
          {
            name: 'Grupos',
            key: 'configuration.users.groups',
          },
          {
            name: 'Usuarios',
            key: 'configuration.users.users',
          },
          {
            name: 'Bitacora',
            key: 'configuration.users.log',
          },
        ],
      },
    ],
  },
  {
    name: 'Facturación',
    key: "invoice",
    children: [
      {
        name: "Nuevo cliente",
        key: "invoice.newClient",
      },
      {
        name: "Editar cliente",
        key: "invoice.editClient"
      },
      {
        name: "Nuevo producto",
        key: "invoice.newProduct"
      },
      {
        name: "Eliminar producto",
        key: "invoice.deleteProduct"
      },
    ],
  },
];
