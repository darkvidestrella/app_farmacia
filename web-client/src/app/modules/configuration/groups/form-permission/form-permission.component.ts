import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { AjaxService } from 'src/app/services/ajax.service';
import { PermissionNode, TREE_DATA } from './permission-definition';


/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-form-permission',
  templateUrl: './form-permission.component.html',
  styleUrls: ['./form-permission.component.scss'],
  providers: [AjaxService],
})
export class FormPermissionComponent implements OnInit {

  menuDefinition = {
    "dashboard": false,
    "configuration": false,
    "configuration.dashboard": false,
    "configuration.general": false,
    "configuration.general.laboratories": false,
    "configuration.general.laboratories.add": false,
    "configuration.general.laboratories.edit": false,
    "configuration.general.laboratories.delete": false,
    "configuration.general.products": false,
    "configuration.general.products.add": false,
    "configuration.general.products.edit": false,
    "configuration.general.products.downloadFile": false,
    "configuration.general.products.uploadFile": false,
    "configuration.movements.list": false,
    "configuration.movements.register": false,
    "configuration.movements.register.income": false,
    "configuration.movements.register.outcome": false,
    "configuration.movements.register.upload": false,
    "configuration.users": false,
    "reports.invoices": false,
    "reports.kardex": false,
    "reports.conrtolledProducts": false,
    "users.groups": false,
    "users.groups.add": false,
    "users.groups.edit": false,
    "users.groups.delete": false,
    "users.groups.permissions": false,
    "users.users": false,
    "users.users.add": false,
    "users.users.edit": false,
    "users.users.delete": false,
    "invoice": false,
    "invoice.newClient": false,
    "invoice.editClient": false,
    "invoice.newProduct": false,
  };
  group = null;

  private _transformer = (node: PermissionNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      key: node.key,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(
    public dialogRef: MatDialogRef<FormPermissionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ajax: AjaxService,
    private snackbar: MatSnackBar
  ) {
    this.group = data;
    this.dataSource.data = TREE_DATA;
    if (this.group.menu !== '') {
      this.menuDefinition = {
        ...this.menuDefinition,
        ...JSON.parse(data.menu),
      };
    }
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  ngOnInit(): void {
  }

  updatePermission(event: any, nodeKey: string) {
    this.menuDefinition[nodeKey] = event.checked;
  }

  savePermissions() {
    this.ajax.put("/permissions", {
      _id: this.group._id,
      menu: JSON.stringify(this.menuDefinition),
    }).subscribe(result => {
      if (result.msg === 'success') {
        this.dialogRef.close(true);
      } else {
        this.snackbar.open(result.message, 'Cerrar');
      }
    })
  }

}
