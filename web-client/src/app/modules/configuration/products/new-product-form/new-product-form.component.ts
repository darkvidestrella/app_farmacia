import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AjaxService } from '../../../../services/ajax.service';
import { ConfirmDialogComponent } from '../../../../shared/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-new-product-form',
  templateUrl: './new-product-form.component.html',
  styleUrls: ['./new-product-form.component.scss'],
  providers: [AjaxService]
})
export class NewProductFormComponent implements OnInit {
  @Input() entryProductBarcode: string;
  @Output() productSaved = new EventEmitter();
  psicoTypes = [
    { value: 'medicamento', name: 'Sin definir' },
    { value: 'precursores', name: 'Precursores' },
    { value: 'psicotropicos', name: 'Psicotropicos' },
    { value: 'estupefacientes', name: 'Estupefacientes' }
  ];

  form: FormGroup;
  laboratories = [];

  constructor(
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    private ajax: AjaxService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<NewProductFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      barcode: ['', Validators.required],
      description: '',
      prescription: '',
      dosis: '',
      quantityPerBox: [1, Validators.min(1)],
      laboratory: '',
      unitMinimumAlert: [0, Validators.min(0)],
      restricted: false,
      price: 0,
      restrictedType: '',
      payIva: false,
      priceDisscount: 0
    });
  }

  ngOnInit() {
    if (this.entryProductBarcode) {
      this.form.get('barcode').setValue(this.entryProductBarcode);
    }
    this.listLaboratories();
  }

  listLaboratories() {
    this.ajax.get('/laboratories').subscribe(result => {
      if (result.msg === 'success') {
        this.laboratories = result.data.map(laboratory => {
          return {
            name: laboratory.name,
            _id: laboratory.name
          };
        });
      }
    });
  }

  save() {
    if (this.form.valid) {
      this.ajax.post('/products', this.form.value).subscribe(result => {
        if (result.msg === 'success') {
          const dialog = this.dialog.open(ConfirmDialogComponent, {
            data: {
              title: 'Agregado exitosamente',
              text: 'Su producto fue creado exitosamente',
              cancelButton: false
            }
          });
          dialog.afterClosed().subscribe(() => {
            this.productSaved.emit(result);
          });
        } else {
          this.snackbar.open(result.message, 'Cerrar');
        }
      });
    } else {
      this.snackbar.open('Revise sus datos y vuelva a intentar', 'Cerrar');
    }
  }

  doCancel() {
    this.form.reset();
    this.productSaved.emit({
      msg: 'canceled'
    });
    if (this.data.closeOnCancel === true) {
      this.dialogRef.close();
    }
  }
}
