import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { FormGroup, FormBuilder } from "@angular/forms";

import { BehaviorSubject } from "rxjs";

import { AjaxService } from "../../../services/ajax.service";
import { Product } from "../../../interface/product.interface";
import { ConfirmDialogComponent } from "../../../shared/confirm-dialog/confirm-dialog.component";
import { NewProductFormComponent } from "./new-product-form/new-product-form.component";
import { EditProductFormComponent } from "./edit-product-form/edit-product-form.component";
import { downloadFile, launchPrint } from "../../../helpers/app.helpers";
import { shareReplay } from "rxjs/operators";
import * as moment from 'moment';


@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.scss"],
  providers: [AjaxService],
})
export class ProductsComponent {
  displayedColumns = [
    "barcode",
    "name",
    "laboratory",
    "expiration",
    "price",
    "priceDisscount",
    "unitMinimumAlert",
    "boxes",
    "_id",
  ];
  productsData$ = new BehaviorSubject([]);
  productDataSource: MatTableDataSource<Product> = new MatTableDataSource([]);
  filterText: string;
  uploadForm: FormGroup;
  loading = false;

  @ViewChild("filter", { static: true }) filter: ElementRef<HTMLInputElement>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild("fileButton", { static: true }) fileButton: ElementRef<
    HTMLButtonElement
  >;
  @ViewChild("submitButton", { static: true }) submitButton: ElementRef<
    HTMLButtonElement
  >;
  @ViewChild("reportContent", { static: true }) reportContent: ElementRef<
    HTMLElement
  >;

  constructor(
    private ajax: AjaxService,
    private form: MatDialog,
    public snackBar: MatSnackBar,
    private dialog: MatDialog,
    private fb: FormBuilder
  ) {
    this.filterText = "";
    this.productsData$.subscribe((products: Product[]) => {
      this.productDataSource.data = products;
      this.productDataSource.sort = this.sort;
      this.productDataSource.paginator = this.paginator;
    });
    this.uploadForm = this.fb.group({ file: [null] });
  }

  applyFilter(filterText?: string) {
    this.productDataSource.filter = filterText.trim().toLowerCase();
  }

  printReport() {
    const contentCopy = this.reportContent.nativeElement.cloneNode(true) as HTMLElement;
    const elementsToHide = contentCopy.querySelectorAll(".no-print");
    elementsToHide.forEach(element => element.remove());
    launchPrint("Listado de productos", contentCopy.innerHTML);
  }

  listProducts() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.ajax
      .get("/products", {
        limit: -1,
      })
      .pipe(shareReplay(1))
      .subscribe(
        (products) => {
          const data = products.data.map(product => {
            const details = product.detailedMovements.map(detail => {
              return {
                expirationDate: detail.expirationDate,
                lote: detail.lote,
              };
            });
            return {
              ...product,
              expiration: details,
            };
          });
          this.productsData$.next(data);
          this.loading = false;
        },
        (error) => {
          console.log(error.message);
          this.loading = false;
        }
      );
  }

  openForm(_id: string, currentProduct?: Product) {
    let product: Product = {
      _id: "",
      name: "",
      description: "",
      barcode: "",
      companyId: "",
      dosis: "",
      laboratory: "",
      photo: "",
      prescription: "",
      quantityPerBox: 0,
      restricted: false,
      unitMinimumAlert: 0,
      // expiration: '',
    };
    if (_id !== "") {
      product = {
        _id: currentProduct._id,
        name: currentProduct.name,
        description: currentProduct.description,
        barcode: currentProduct.barcode,
        companyId: currentProduct.companyId,
        dosis: currentProduct.dosis,
        laboratory: currentProduct.laboratory,
        prescription: currentProduct.prescription,
        quantityPerBox: currentProduct.quantityPerBox,
        restricted: currentProduct.restricted,
        unitMinimumAlert: currentProduct.unitMinimumAlert,
        // expiration: currentProduct?.expiration,
      };
    }
    const dialog = this.form.open(NewProductFormComponent, {
      data: {
        product,
        closeOnCancel: true,
      },
    });
    dialog.afterClosed().subscribe((data) => {
      this.listProducts();
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  deleteProduct(_id: string) {
    const dialog = this.form.open(ConfirmDialogComponent, {
      disableClose: true,
      data: {
        title: "Eliminar",
        text: "Confirme que desea eliminar?",
      },
    });
    dialog.afterClosed().subscribe((result) => {
      if (result === true) {
        this.ajax.delete("/products", { _id }).subscribe(() => {
          this.listProducts();
        });
      }
    });
  }

  editProduct(product) {
    const editedProduct = {
      _id: product._id,
      name: product.name || "",
      barcode: product.barcode || "",
      description: product.description || "",
      prescription: product.prescription || "",
      dosis: product.dosis || "",
      quantityPerBox: product.quantityPerBox || 0,
      laboratory: product.laboratory || "",
      unitMinimumAlert: product.unitMinimumAlert || 1,
      restricted: product.restricted || false,
      price: product.price || 0,
      restrictedType: product.restrictedType || "medicamento",
      payIva: product.payIva || false,
      priceDisscount: product.priceDisscount || product.price,
      expiration: product.expiration || '',
    };
    const dialog = this.dialog.open(EditProductFormComponent, {
      data: editedProduct,
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.listProducts();
      }
    });
  }

  downloadXls() {
    this.ajax
      .get("/products/list/download", {
        filter: this.filterText,
      })
      .subscribe((response) => {
        if (response.data.error === "") {
          downloadFile(response.data.file);
        } else {
          this.snackBar.open(response.data.error, "Cerrar");
        }
      });
  }

  checkFileSelected(event) {
    if (event.target.files.length > 0) {
      const [file] = event.target.files;
      this.uploadForm.patchValue({
        file,
      });
      this.submitButton.nativeElement.click();
      setTimeout(() => {
        this.uploadForm.patchValue({
          fil: null,
        });
      }, 1000);
    }
  }

  uploadFileToServer() {
    const formData: FormData = new FormData();
    formData.append(
      "file",
      this.uploadForm.value.file,
      this.uploadForm.value.file.name
    );
    this.ajax.post("/products/upload-file", formData).subscribe((result) => {
      if (result.msg === "success") {
        this.listProducts();
      } else {
        this.snackBar.open(result.message, "Cerrar");
      }
    });
  }
}
