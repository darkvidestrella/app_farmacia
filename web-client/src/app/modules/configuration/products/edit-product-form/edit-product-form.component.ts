import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AjaxService } from '../../../../services/ajax.service';
import { roundValue } from '../../../../helpers/app.helpers';

@Component({
  selector: 'app-edit-product-form',
  templateUrl: './edit-product-form.component.html',
  styleUrls: ['./edit-product-form.component.scss'],
  providers: [AjaxService]
})
export class EditProductFormComponent implements OnInit {
  form: FormGroup;
  laboratories = [];

  psicoTypes = [
    { value: 'medicamento', name: 'Sin definir' },
    { value: 'precursores', name: 'Precursores' },
    { value: 'psicotropicos', name: 'Psicotropicos' },
    { value: 'estupefacientes', name: 'Estupefacientes' }
  ];

  constructor(
    private fb: FormBuilder,
    private snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EditProductFormComponent>,
    private ajax: AjaxService
  ) {
    this.form = this.fb.group({
      _id: ['', Validators.required],
      name: ['', Validators.required],
      barcode: ['', Validators.required],
      description: '',
      prescription: '',
      dosis: '',
      quantityPerBox: [1, Validators.min(1)],
      laboratory: '',
      unitMinimumAlert: [0, Validators.min(0)],
      restricted: false,
      price: [0, [Validators.min(0), Validators.required]],
      restrictedType: 'medicamento',
      payIva: false,
      priceDisscount: [0, Validators.min(0)]
    });
  }

  listLaboratories() {
    this.ajax.get('/laboratories').subscribe(result => {
      if (result.msg === 'success') {
        this.laboratories = result.data.map(laboratory => {
          return {
            name: laboratory.name,
            _id: laboratory.name
          };
        });
      }
    });
  }

  ngOnInit() {
    this.listLaboratories();
    if (!('payIva' in this.data)) {
      this.data = {
        ...this.data,
        payIva: false
      };
    }
    this.data = {
      ...this.data,
      price: roundValue(this.data.price)
    };
    this.form.setValue(this.data);
  }

  updateProduct() {
    if (!this.form.valid) {
      this.snackbar.open(
        'El producto esta con errores, revise sus datos antes de guardar',
        'Cerrar'
      );
      return;
    }
    this.ajax.put('/products', this.form.value).subscribe(result => {
      if (result.msg === 'success') {
        this.dialogRef.close(true);
      } else {
        this.snackbar.open(result.message, 'Cerrar');
      }
    });
  }
}
