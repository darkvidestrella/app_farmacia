import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AjaxService } from '../../../../services/ajax.service';

@Component({
  selector: 'app-form-laboratory',
  templateUrl: './form-laboratory.component.html',
  styleUrls: ['./form-laboratory.component.scss'],
  providers: [AjaxService]
})
export class FormLaboratoryComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<FormLaboratoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ajax: AjaxService,
    private fb: FormBuilder,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      _id: '',
      name: ['', Validators.required],
      address: '',
      telephone: ''
    });
  }

  closeForm() {
    this.form.reset();
    this.dialogRef.close();
  }

  save() {
    if (this.form.valid) {
      if (this.form.value._id && this.form.value._id !== '') {
        this.ajax.put('/laboratories', this.form.value).subscribe(result => {
          if (result.msg === 'success') {
            this.dialogRef.close(true);
          } else {
            this.snackbar.open(result.message, 'Cerrar');
          }
        });
      } else {
        const laboratoryInfo = {
          name: this.form.value.name,
          address: this.form.value.address,
          telephone: this.form.value.telephone
        };
        this.ajax.post('/laboratories', laboratoryInfo).subscribe(result => {
          if (result.msg === 'success') {
            this.dialogRef.close(true);
          } else {
            this.snackbar.open(result.message, 'Cerrar');
          }
        });
      }
    } else {
      this.snackbar.open('El formulario tiene errores', 'Cerrar');
    }
  }

  ngOnInit() {
    if (this.data) {
      this.form.setValue({
        _id: this.data['_id'] || '',
        name: this.data['name'] || '',
        address: this.data['address'] || '',
        telephone: this.data['telephone'] || ''
      });
    }
  }
}
