import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatTableDataSource } from "@angular/material/table";
import { Laboratory } from "../../../interface/laboratory";
import { AjaxService } from "../../../services/ajax.service";
import { ConfirmDialogComponent } from "../../../shared/confirm-dialog/confirm-dialog.component";
import { FormLaboratoryComponent } from "./form-laboratory/form-laboratory.component";

@Component({
  selector: "app-laboratories",
  templateUrl: "./laboratories.component.html",
  styleUrls: ["./laboratories.component.scss"],
  providers: [AjaxService],
})
export class LaboratoriesComponent implements OnInit {
  form: FormGroup;
  laboratoriesDataSource = new MatTableDataSource();
  displayedColumns = ["name", "address", "telephone", "_id"];
  checkExistence = false;
  runFirstLoad = false;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild("filterText", { static: true }) filterText: ElementRef<
    HTMLInputElement
  >;

  constructor(
    private fb: FormBuilder,
    private ajax: AjaxService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      name: ["", Validators.required],
      address: "",
      telephone: "",
    });
  }

  list() {
    const element = this.filterText.nativeElement;
    if (element) {
      this.laboratoriesDataSource.data = [];
      this.ajax
        .get("/laboratories", {
          filter: element.value || "",
        })
        .subscribe((response) => {
          if (response.msg === "success") {
            this.laboratoriesDataSource.data = response.data;
            this.laboratoriesDataSource.paginator = this.paginator;
            this.checkExistenceLaboratories();
          }
        });
    }
  }

  delete(_id: string) {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Eliminar laboratorio?",
        text: "Esta seguro que desea eliminar este laboratorio",
        cancelButton: true,
      },
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.ajax
          .delete("/laboratories", {
            _id,
          })
          .subscribe((response) => {
            if (response.msg === "success") {
              this.list();
            } else {
              this.snackbar.open(response.message, "Cerrar");
            }
          });
      }
    });
  }

  openForm(laboratory: any) {
    const dialog = this.dialog.open(FormLaboratoryComponent, {
      data: laboratory || {},
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.list();
      }
    });
  }

  applyFilter() {
    this.list();
  }

  ngOnInit() {
    this.list();
  }

  checkExistenceLaboratories() {
    if (this.checkExistence === false) {
      this.checkExistence = true;
      if (
        this.laboratoriesDataSource &&
        this.laboratoriesDataSource.data &&
        this.laboratoriesDataSource.data.length === 0
      ) {
        this.runFirstLoad = true;
      } else {
        this.runFirstLoad = false;
      }
    }
  }

  firstImport() {
    this.ajax.get("/laboratories/first-load").subscribe(() => {
      this.runFirstLoad = false;
      this.list();
    });
  }
}
