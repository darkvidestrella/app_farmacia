import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { launchPrint } from 'src/app/helpers/app.helpers';
import { AjaxService } from 'src/app/services/ajax.service';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-expired-products',
  templateUrl: './expired-products.component.html',
  styleUrls: ['./expired-products.component.scss']
})
export class ExpiredProductsComponent implements OnInit {

  searchControl = new FormControl('');
  filteredProducts = new MatTableDataSource([]);
  products = new MatTableDataSource([]);
  displayedColumns = ['name', 'barcode', 'price'];
  loading = false;

  @ViewChild("reportContent", { static: true }) reportContent: ElementRef<
    HTMLElement
  >;

  constructor(
    private ajax: AjaxService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.searchControl.valueChanges.subscribe(value => {
      this.filterProducts(value);
    });
  }

  filterProducts(searchTerm: string) {
    if (searchTerm) {
      this.filteredProducts.data = this.products.data.filter(product =>
        product.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
    } else {
      this.filteredProducts.data = [...this.products.data];
    }
  }

  listProducts() {
    this.loading = true;
    if (this.searchControl.value?.trim() === '') {
      const dialog = this.dialog.open(ConfirmDialogComponent, {
        disableClose: true,
        data: {
          title: "No hay filtro por producto",
          text: "El filtro por producto es vacio, desea cargar todos los registro del reporte, esto puede tomar algunos minutos, desea continuar?",
          cancelButton: true,
        },
      });
      dialog.afterClosed().subscribe((result) => {
        if (result === true) {
          this.loadReport();
        } else {
          this.loading = false;
        }
      });
    } else {
      this.loadReport()
    }
  }

  loadReport() {
    setTimeout(() => {
      this.ajax.get('/products/expired-report', { search: this.searchControl.value.trim() }).subscribe(result => {
        this.products.data = result.data.map(p => ({...p, expanded: false}));
        this.filterProducts(this.searchControl.value || '');
        this.loading = false;
      });
    }, 400);
  }

  isExpired(creationDate: string): boolean {
    const creation = new Date(creationDate);
    return creation < new Date();
  }

  isNearExpiry(creationDate: string): boolean {
    const creation = new Date(creationDate);
    const now = new Date();
    const oneWeekFromNow = new Date();
    oneWeekFromNow.setDate(now.getDate() + 7);
    return creation > now && creation <= oneWeekFromNow;
  }

  isSafe(creationDate: string): boolean {
    return !this.isExpired(creationDate) && !this.isNearExpiry(creationDate);
  }

  printReport() {
    launchPrint(
      "Reporte de facturas",
      this.reportContent.nativeElement.innerHTML
    );
  }

}
