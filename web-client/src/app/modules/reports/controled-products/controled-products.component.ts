import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { MatTableDataSource } from "@angular/material/table";
import { DateTime, launchPrint } from "../../../helpers/app.helpers";
import { AjaxService } from "../../../services/ajax.service";

@Component({
  selector: "app-controled-products",
  templateUrl: "./controled-products.component.html",
  styleUrls: ["./controled-products.component.scss"],
  providers: [AjaxService],
})
export class ControledProductsComponent implements OnInit {
  form: FormGroup;
  productsDataSource = new MatTableDataSource();
  displayedColumns = [
    "barcode",
    "name",
    "restrictedType",
    "pastQty",
    "incomes",
    "outs",
    "calculated",
    "final",
  ];
  currentMonth = 0;
  @ViewChild("printableContent", { static: true })
  printableContent: ElementRef<HTMLElement>;

  constructor(private fb: FormBuilder, private ajax: AjaxService) {
    const startDate = DateTime().startOf("month");
    const endDate = DateTime().endOf("month");
    this.form = this.fb.group({ startDate, endDate, includeDetail: false });
  }

  ngOnInit() {}

  search() {
    this.productsDataSource.data = [];
    this.ajax
      .get("/products/controled-report", this.form.value)
      .subscribe((result) => {
        if (result.msg === "success") {
          this.productsDataSource.data = result.data.map((item) => {
            let totalMovements = 0;
            item.movements.forEach((movement) => {
              if (movement.movementType.toLowerCase() === "ingreso") {
                totalMovements += movement["movedQty"];
              } else {
                totalMovements += movement["movedQty"] * -1;
              }
            });
            return {
              ...item,
              totalMovements,
              movements: item.movements.map((movement) => {
                let additionalInfo = {};
                try {
                  additionalInfo = JSON.parse(movement.additionalInformation);
                } catch (ex) {
                  additionalInfo = {};
                }
                return {
                  ...movement,
                  movementInfo: additionalInfo,
                  documentNumber:
                    movement.movementType.toLowerCase() === "factura"
                      ? additionalInfo["invoiceNumber"]
                      : additionalInfo["movementNumber"],
                };
              }),
            };
          });
        }
      });
  }

  print() {
    launchPrint(
      "Reporte Controlados",
      this.printableContent.nativeElement.innerHTML
    );
  }
}
