import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControledProductsComponent } from './controled-products.component';

describe('ControledProductsComponent', () => {
  let component: ControledProductsComponent;
  let fixture: ComponentFixture<ControledProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControledProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControledProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
