import { Component, OnInit } from '@angular/core';
import { AjaxService } from 'src/app/services/ajax.service';

@Component({
  selector: 'app-log-report',
  templateUrl: './log-report.component.html',
  styleUrls: ['./log-report.component.scss'],
  providers: [AjaxService],
})
export class LogReportComponent implements OnInit {

  logs: any[] = [];
  displayedColumns: string[] = ['user', 'endpoint', 'method', 'statusCode', 'timestamp'];
  selectedUserId: string = '';
  users: any[] = [];

  constructor(private ajax: AjaxService) {}

  ngOnInit(): void {
    this.listUsers();
  }

  getModuleName(endpoint: string) {
    const modules = {
      "/permissions": {module: "Permisos"},
      "/configuration": {module: "Configuracion"},
      "/products": {module: "Productos"},
      "/users": {module: "Usuarios"},
      "/groups": {module: "Grupos"},
      "/invoices": {module: "Facturacion"},
      "/find-invoice": {module: "Buscar factura"},
      "/invoice/sells-report": {module: "Reporte de facturas"},
      "/movements": {module: "Movimientos"},
      "/inventory/movements/list": {module: "Listado de movimientos"},
      "/laboratories": {module: "Laboratorios"},
      "/products/controled-report": {module: "Reporte de productos controlados"},
      "/movement/details/list": {module: "Reporte de movimientos"},
      "/inventory/kardex": {module: "Reporte de kardex"},
      "/clients": {module: "Clientes"},
      "/products/expired-report": {module: "Reporte de producto expirados"},
      "/invoices/day": {module: "Busqueda de facturas por fecha"},
      "/invoices/daily/sells": {module: "Ventas diarias"},
      "/products/minimun": {module: "Reporte de minimos"},
      "/logs": {module: "Bitacora"},
      "/invoice/annulation": {module: "Anulacion Facturas"},
  };
    const segment = endpoint.split('?');
    const key = (segment[0] || '/').replace('/api', '');
    return modules?.[key]?.module || key;
  }

  loadReport() {
    this.ajax.get('/logs', { user: this.selectedUserId }).subscribe(logs => {
      this.logs = logs.data.map(log => {
        return {
          ...log,
          endpoint: this.getModuleName(log.endpoint),
        };
      });
    });
  }

  listUsers() {
    this.ajax.get('/users').subscribe(users => {
      this.users = users.data;
    });
  }

}
