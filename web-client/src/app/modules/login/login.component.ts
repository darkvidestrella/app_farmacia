// login.component.ts
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AjaxService } from 'src/app/services/ajax.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AjaxService],
})
export class LoginComponent {
  loginForm: FormGroup;
  submitted = false;
  hasSession = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private ajax: AjaxService,
    private snackbar: MatSnackBar,
    private router: Router,
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
    this.hasSession = this.authService.isAuthenticated();
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    const token = 'dummy-token';
    this.ajax.post('/login', {
      ...this.loginForm.value,
    }).subscribe(result => {
      if (result.msg === 'success' && result?.token) {
        this.authService.login(result.token, result?.isAdmin);
        this.router.navigate(['/dasboard'])
        window.location.href = '/';
      } else {
        this.snackbar.open(result.message, 'Cerrar');
      }
    })
  }
}
