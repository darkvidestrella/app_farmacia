import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatTableDataSource } from "@angular/material/table";

import { Product } from "../../interface/product.interface";
import { AjaxService } from "../../services/ajax.service";
import { PrintModalComponent } from "./print-modal/print-modal.component";
import { ChangeCalculatorComponent } from "./change-calculator/change-calculator.component";
import { environment } from "../../../environments/environment";

@Component({
  selector: "app-invoices",
  templateUrl: "./invoices.component.html",
  styleUrls: ["./invoices.component.scss"],
  providers: [AjaxService],
})
export class InvoicesComponent implements OnInit {
  clientSelected: any;
  productsDataSource = new MatTableDataSource();
  displayedColumns = ["_id", "name", "boxes", "units", "price", "subtotal"];
  editedProduct: any;
  invoice = {
    clientId: "",
    date: "",
    client: {
      dni: "",
      name: "",
      address: "",
      telephone: "",
      email: "",
    },
    prescription: {
      number: "",
      pacientName: "",
      dni: "",
    },
    disscountApplied: false,
    products: [],
    subtotal: 0,
    iva_pct: 0,
    iva: 0,
    disscount: 0,
    total: 0,
  };
  emiting = false;
  applyDisscount = false;
  meddicalPrescriptionRequired = false;

  @ViewChild("clientSection", { static: true }) clientSection: ElementRef<
    HTMLElement
  >;

  constructor(
    private snackbar: MatSnackBar,
    private ajax: AjaxService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.editedProduct = {};
    this.calculateTotals();
  }

  calculateTotals() {
    this.productsDataSource.connect().subscribe((items) => {
      const subtotal: number = items.reduce((lastValue, item) => {
        this.meddicalPrescriptionRequired = item["restricted"] === true;
        return lastValue + item["subtotal"];
      }, 0) as number;
      const subtotal_disscount = items.reduce((lastValue, item) => {
        return lastValue + item["subtotal_disscount"];
      }, 0) as number;
      if (this.invoice.disscountApplied === true) {
        this.invoice.iva = items.reduce((lastValue, item) => {
          return Number(lastValue + (item["iva_disscount"] || 0));
        }, 0) as number;
        this.invoice.subtotal = Number(subtotal_disscount - this.invoice.iva);
        this.invoice.disscount = Number(subtotal - subtotal_disscount);
      } else {
        this.invoice.iva = items.reduce((lastValue, item) => {
          return lastValue + (item["iva"] || 0);
        }, 0) as number;
        this.invoice.subtotal = subtotal - this.invoice.iva;
        this.invoice.disscount = 0;
      }
      this.invoice.iva_pct = environment.IVA;
      this.invoice.total = this.invoice.subtotal + this.invoice.iva;
    });
  }

  fillClientData(event) {
    this.clientSelected = event;
    this.invoice.clientId = event._id;
    const cleanClient = {
      ...this.clientSelected,
    };
    delete cleanClient._id;
    this.invoice.client = {
      ...cleanClient,
    };
  }

  addProductItem(event) {
    const currentProducts = this.productsDataSource.data;
    const exists = currentProducts.find((item: any) => item._id === event._id);
    if (exists) {
      const updatedProducts = currentProducts.map((item: Product) => {
        if (item._id === event._id) {
          // validate stock
          const totalUnitsRegistered = (item.boxes + event.boxes) * item.quantityPerBox + (item.units + event.units);
          if (totalUnitsRegistered > item.totalUnits) {
            this.snackbar.open(
              `No tiene stock suficiente para seleccionar ${totalUnitsRegistered} unidad(es) (Disponible: ${item.totalUnits})`,
              "Cerrar"
            );
            return exists;
          }
          return {
            ...item,
            boxes: item.boxes + event.boxes,
            units: item.units + event.units,
            subtotal: totalUnitsRegistered * item.unit_price,
            subtotal_disscount: totalUnitsRegistered * item.unit_price_disscount,
          };
        } else {
          return item;
        }
      });
      this.productsDataSource.data = updatedProducts;
    } else {
      currentProducts.push(event);
      this.productsDataSource.data = currentProducts;
    }
  }

  editCurrentProduct(product) {
    const clickedProduct = this.productsDataSource.data.find(
      (item: any) => item._id === product._id
    );
    const newList = this.productsDataSource.data.filter(
      (item: any) => item._id !== product._id
    );
    this.productsDataSource.data = newList;
    this.editedProduct = clickedProduct;
  }

  removeProduct(id: string) {
    const newList = this.productsDataSource.data.filter(
      (item: any) => item._id !== id
    );
    this.productsDataSource.data = newList;
  }

  emitInvoice() {
    this.emiting = true;
  }

  cleanClient() {
    this.invoice.clientId = "";
    this.clientSelected = {
      _id: "",
      dni: "0000000000",
      firstName: "Final",
      lastName: "Consumidor",
      address: "",
      telephone: "",
      email: "",
    };
    const cleanClient = {
      ...this.clientSelected,
    };
    delete cleanClient._id;
    this.invoice.client = { ...cleanClient };
  }

  saveInvoice() {
    if (this.invoice.client.dni === "") {
      this.snackbar.open("No ha ingresado un cliente", "Cerrar");
      return;
    }
    if (this.meddicalPrescriptionRequired === true) {
      if (this.invoice.prescription.dni === "") {
        this.snackbar.open(
          "El número de cédula del cliente es requerido para la receta",
          "Cerrar"
        );
        return;
      }
      if (this.invoice.prescription.number === "") {
        this.snackbar.open("El número de receta es requerido", "Cerrar");
        return;
      }
      if (this.invoice.prescription.pacientName === "") {
        this.snackbar.open(
          "El nombre del paciente es requerido para la receta",
          "Cerrar"
        );
        return;
      }
    }
    this.processInvoice(true);
    // const dialog = this.dialog.open(ChangeCalculatorComponent, {
    //   data: this.invoice.total,
    // });
    // dialog.afterClosed().subscribe((result) => {
    //   this.processInvoice();
    // });
  }

  processInvoice(printDirectly = false) {
    this.invoice.products = this.productsDataSource.data;
    this.ajax.post("/invoices", this.invoice).subscribe((data) => {
      if (data.msg === "success") {
        this.doPrint(data.data.invoiceNumber, printDirectly);
        this.cleanInvoice();
      } else {
        this.snackbar.open(data.message, "Cerrar");
      }
    });
  }

  cleanInvoice() {
    this.emiting = false;
    this.meddicalPrescriptionRequired = false;
    this.invoice = {
      clientId: "",
      date: "",
      client: {
        dni: "",
        name: "",
        address: "",
        telephone: "",
        email: "",
      },
      prescription: {
        number: "",
        pacientName: "",
        dni: "",
      },
      disscountApplied: false,
      products: [],
      subtotal: 0,
      iva_pct: 0,
      iva: 0,
      disscount: 0,
      total: 0,
    };
    this.productsDataSource.data = [];
    this.cleanClient();
  }

  doPrint(invoiceNumber?: string, printDirectly: boolean = false) {
    const dialog = this.dialog.open(PrintModalComponent, {
      width: "80%",
      height: "80%",
      data: {
        invoiceNumber,
        printDirectly,
      },
    });
  }
}
