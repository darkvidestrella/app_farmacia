import {
  Component,
  OnInit,
  Inject,
  Input,
  ViewChild,
  ElementRef,
} from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AjaxService } from "../../../services/ajax.service";
import { launchPrint } from "../../../helpers/app.helpers";
import { ConfirmDialogComponent } from "../../../shared/confirm-dialog/confirm-dialog.component";

@Component({
  selector: "app-print-modal",
  templateUrl: "./print-modal.component.html",
  styleUrls: ["./print-modal.component.scss"],
  providers: [AjaxService],
})
export class PrintModalComponent implements OnInit {
  @Input() invoiceNumber: string;
  @Input() printDirectly: boolean;
  @ViewChild("htmlPrintContent", { static: false }) htmlPrintContent: ElementRef<HTMLElement>;
  printedInvoice: any;
  invoicesList: any[];
  invoiceTemplate: string;
  constructor(
    public dialogRef: MatDialogRef<PrintModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ajax: AjaxService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit() {
    this.invoiceNumber = this.data.invoiceNumber || "";
    this.printDirectly = this.data.printDirectly || false;
    if (this.invoiceNumber !== "") {
      this.findInvoice();
    }
  }

  doFindInvoice(event) {
    if ("key" in event && event.key.toLowerCase() === "enter") {
      this.findInvoice();
    }
  }

  findInvoice() {
    this.invoicesList = [];
    if (this.invoiceNumber === "") {
      return;
    }
    this.ajax
      .get("/find-invoice", {
        invoiceNumber: this.invoiceNumber,
      })
      .subscribe((invoice) => {
        if (invoice.msg === "success") {
          const exists = invoice.data.length;
          if (exists === 1) {
            this.invoicesList = invoice.data;
            this.printedInvoice = {
              ...invoice.data[0],
            };
            if (this.printDirectly) {
              setTimeout(() => {
                this.printInvoice();
              }, 500);
            }
          } else if (exists > 1) {
            this.invoicesList = invoice.data;
          }
        }
      });
  }

  selectInvoice(invoiceNumber: string) {
    this.invoiceNumber = invoiceNumber;
    this.findInvoice();
  }

  printInvoice() {
    launchPrint(
      "Impresion de factura",
      this.htmlPrintContent.nativeElement.innerHTML
    );
  }

  invoiceAnnulation() {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "Anulación de facturas",
        text: `Esta seguro que desea anular la factura ${this.printedInvoice.invoiceNumber} del cliente ${this.printedInvoice.client.lastName} ${this.printedInvoice.client.firstName}?`,
        cancelButton: true,
      },
    });
    dialog.afterClosed().subscribe((result) => {
      if (result) {
        this.ajax
          .put("/invoice/annulation", {
            _id: this.printedInvoice._id,
          })
          .subscribe((response) => {
            if (response.msg === "success") {
              this.dialogRef.close();
            } else {
              this.snackbar.open(response.message, "Cerrar");
            }
          });
      }
    });
  }
}
