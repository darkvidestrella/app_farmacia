import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import {
  FormGroup,
  FormBuilder,
} from "../../../../../node_modules/@angular/forms";
import { DateTime, launchPrint } from "../../../helpers/app.helpers";
import { AjaxService } from "../../../services/ajax.service";
import { MatDialog } from "@angular/material/dialog";
import { PrintModalComponent } from "../print-modal/print-modal.component";

@Component({
  selector: "app-invoices-report",
  templateUrl: "./invoices-report.component.html",
  styleUrls: ["./invoices-report.component.scss"],
  providers: [AjaxService],
})
export class InvoicesReportComponent implements OnInit {
  form: FormGroup;
  invoicesDataSource = new MatTableDataSource();
  displayedColumns = [
    "item",
    "invoiceNumber",
    "date",
    "subtotal",
    "disscount",
    "iva",
    "total",
  ];
  summary = {
    subtotal: 0,
    disscount: 0,
    iva: 0,
    iva_pct: 0,
    total: 0,
  };

  @ViewChild("reportContent", { static: true }) reportContent: ElementRef<
    HTMLElement
  >;

  constructor(
    private fb: FormBuilder,
    private ajax: AjaxService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    const startDate = DateTime();
    const endDate = DateTime();
    this.form = this.fb.group({
      startDate,
      endDate,
    });
  }

  searchInvoices() {
    this.invoicesDataSource.data = [];
    this.summary = {
      subtotal: 0,
      disscount: 0,
      iva: 0,
      iva_pct: 0,
      total: 0,
    };
    let item = 0;
    this.ajax
      .get("/invoice/sells-report", this.form.value)
      .subscribe((result) => {
        this.invoicesDataSource.data = result.data.map((invoice) => {
          item++;
          if (invoice.status === "emited") {
            this.summary.subtotal += invoice.subtotal;
            this.summary.disscount += invoice.disscount;
            this.summary.iva += invoice.iva;
            this.summary.total += invoice.total;
          }
          return {
            ...invoice,
            item,
          };
        });
      });
  }

  printReport() {
    launchPrint(
      "Reporte de facturas",
      this.reportContent.nativeElement.innerHTML
    );
  }
  showInvoice(invoiceElement) {
    const dialog = this.dialog.open(PrintModalComponent, {
      width: "80%",
      height: "80%",
      data: {invoiceNumber: invoiceElement.invoiceNumber, printDirectly: false},
    });
  }
}
