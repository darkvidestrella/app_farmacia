import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-change-calculator',
  templateUrl: './change-calculator.component.html',
  styleUrls: ['./change-calculator.component.scss']
})
export class ChangeCalculatorComponent implements OnInit {
  totalAmount = 0;
  changeAmount = 0;
  payment = 0;
  correctChange = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ChangeCalculatorComponent>
  ) {}

  ngOnInit() {
    this.totalAmount = Number(this.data);
  }

  calculateChange(event) {
    this.changeAmount = Number(this.totalAmount) - Number(this.payment);
    if (this.changeAmount > 0) {
      this.correctChange = false;
    } else {
      this.correctChange = true;
    }
    if ('key' in event && event.key.toLowerCase() === 'enter') {
      this.closeCalculator();
    }
  }

  closeCalculator() {
    this.dialogRef.close();
  }
}
