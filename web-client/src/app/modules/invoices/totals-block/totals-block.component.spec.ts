import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalsBlockComponent } from './totals-block.component';

describe('TotalsBlockComponent', () => {
  let component: TotalsBlockComponent;
  let fixture: ComponentFixture<TotalsBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalsBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalsBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
