import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-totals-block',
  templateUrl: './totals-block.component.html',
  styleUrls: ['./totals-block.component.scss']
})
export class TotalsBlockComponent implements OnInit {
  @Input()
  invoice = {
    subtotal: 0,
    iva_pct: 0,
    iva: 0,
    disscount: 0,
    total: 0
  };

  constructor() { }

  ngOnInit() {}
}
