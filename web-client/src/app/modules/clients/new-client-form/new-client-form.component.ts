import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AjaxService } from '../../../services/ajax.service';

@Component({
  selector: 'app-new-client-form',
  templateUrl: './new-client-form.component.html',
  styleUrls: ['./new-client-form.component.scss'],
  providers: [AjaxService]
})
export class NewClientFormComponent implements OnInit, OnChanges {
  @Input() showActionButtons = true;
  @Input() entryClient: any;
  @Input() smallView: boolean;
  @Output() buttonClicked = new EventEmitter();

  client = {
    _id: '',
    firstName: '',
    lastName: '',
    email: '',
    dni: '',
    address: '',
    telephone: ''
  };

  cleanClient() {
    this.client = {
      _id: '',
      firstName: '',
      lastName: '',
      email: '',
      dni: '',
      address: '',
      telephone: ''
    };
  }

  constructor(private ajax: AjaxService, private snackbar: MatSnackBar) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.entryClient && changes.entryClient.currentValue) {
      this.client = { ...changes.entryClient.currentValue };
    }
  }

  emitButtonAction(action: string) {
    if (action === 'cancel') {
      this.cleanClient();
      this.buttonClicked.emit({ action, data: {} });
    } else if (action === 'save') {
      this.saveClient();
    }
  }

  saveClient() {
    const client = {
      ...this.client
    };
    if (this.client._id !== '') {
      // update
      this.ajax.put('/clients', this.client).subscribe(result => {
        if (result.msg === 'success') {
          this.buttonClicked.emit({ action: 'success', data: result.data });
        } else {
          this.snackbar.open(result.message, 'Cerrar');
        }
      });
    } else {
      // save
      delete client._id;
      this.ajax.post('/clients', client).subscribe(result => {
        if (result.msg === 'success') {
          this.buttonClicked.emit({ action: 'success', data: result.data });
        } else {
          this.snackbar.open(result.message, 'Cerrar');
        }
      });
    }
  }
}
