import * as moment from 'moment';
import { environment } from '../../environments/environment';

export const DateTime = (date?: string) => {
  return moment();
};

export const launchPrint = (title, content) => {
  const printWindow = window.open('', title);
  printWindow.document.write('<html><head><title> </title>');
  printWindow.document.write('</head><body >');
  printWindow.document.write(content);
  printWindow.document.write('</body></html>');
  printWindow.document.close(); // necessary for IE >= 10
  printWindow.focus(); // necessary for IE >= 10*/
  printWindow.print();
  setTimeout(() => {
    printWindow.close();
  } , 2000);
  return true;
};

export const roundValue = (numberValue: number, decimalValue: number = 2): number => {
  const returnedValue: number = Number(
    Number(
      Math.round(numberValue * Math.pow(10, decimalValue)) / Math.pow(10, decimalValue)
    ).toFixed(2));
  return returnedValue;
};

export const downloadFile = (fileName: string) => {
  window.open(`${environment.serverUrl}/api/download-file/${encodeURIComponent(fileName)}`);
};
